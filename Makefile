
all:
	make compil

compil:
	xelatex -shell-escape -output-directory=./aux_files/ main.tex
	cp ./aux_files/main.pdf ./main.pdf	

full_chap1:
	xelatex -shell-escape -jobname=chap1 -output-directory=./aux_files/ "\input{preamble.tex} \begin{document} \include{chapters/chapter1} \printbibliography[heading=bibintoc]\end{document}"
	
	cd aux_files/ && bibtex chap1.aux && makeindex main.nlo -s nomencl.ist -o main.nls && cd ../
	xelatex -shell-escape -jobname=chap1 -output-directory=./aux_files/ "\input{preamble.tex} \begin{document} \include{chapters/chapter1} \printbibliography[heading=bibintoc]\end{document}"
	xelatex -shell-escape -jobname=chap1 -output-directory=./aux_files/ "\input{preamble.tex} \begin{document} \include{chapters/chapter1} \printbibliography[heading=bibintoc]\end{document}"
	cp ./aux_files/chap1.pdf chap1.pdf

chap1:
	xelatex -shell-escape -jobname=chap1 -output-directory=./aux_files/ "\input{preamble.tex} \begin{document} \include{chapters/chapter1} \end{document}"
	
full:
	make clean
	xelatex -shell-escape -output-directory=./aux_files/ main.tex
	cd aux_files/ && bibtex main.aux && makeindex main.nlo -s nomencl.ist -o main.nls && cd ../
	xelatex -shell-escape -output-directory=./aux_files/ main.tex
	xelatex -shell-escape -output-directory=./aux_files/ main.tex
	cp ./aux_files/main.pdf main.pdf

clean:
	rm -rf aux_files/*
	rm -rf main.pdf
	mkdir aux_files/chapters
	mkdir aux_files/front
	mkdir aux_files/notations
	mkdir aux_files/appendices
	cd aux_files && ln -s ../references/ . && cd ../

