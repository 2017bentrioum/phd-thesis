
\chapter{Introduction}

\label{intro}

\section{Context}

At the time of writing, the world has been hit for about a year by a pandemic of a new coronavirus disease called Covid-19. The pandemic has impacted society a lot, and the media has seized upon two types of questions only scientists were interested in so far:
\begin{enumerate}
    \item How can one infer the spread of the disease based on previously infected people data?
    \item How can we specify properties such as 
          %a country region should benefit reinforced vigilance, the epidemic is stable or over?
          in which region vigilance should be reinforced, the epidemic is stable or over?
\end{enumerate}

The first problem is related to {\em statistical inference} \citep{Casella99}. 
Based on probability theory, statistical inference constructs estimators of properties of a general population from samples, quantifies the uncertainty of such estimation and predicts possible future observations based on available data.

The second problem is related to {\em model checking} \citep{Baier2008}. Based on theoretical computer science, this field groups techniques to check if a model fulfils some specification $\varphi$ expressed by formal logic. 
It provides methods that explore the state-space model efficiently in order to assess whether the model verifies the specification $\varphi$ or not. Initially, it was developed to assess hardware and software systems' reliability, but there is a growing interest in model checking of Systems Biology \citep{Kwiatkowska2008}.

%Monte Carlo methods are a class of algorithmic methods based on simulations of the model which first appears after the WWII (see \citep{Metropolis1987} for the emergence of the method). Given a probability $\Prob[*]$ with density $\pi$, the idea is to sample $x^{(1)}, \ldots, x^{(n)}$ samples from the probability distribution $\Prob[*]$ in order to estimate the integral of $\pi$ (which is the expectation of a random variable with probability distribution $\Prob[*]$. 
%Thus, an important difficulty is to simulate samples from $\pi$ when it is not fully known analytically. Nicholas Metropolis formulates the first Monte Carlo method based on Markov Chains theory \citep{Metropolis1953, Hastings1970} when $\pi$ is known up to a constant.
%Due to the maturity of methods as well as the fast computing power increase of computers, such related Monte Carlo methods are considered standard techniques in the 2000s \citep{Robert2000}.

In this thesis, we focus on statistical inference and verification of stochastic chemical kinetics. Chemical Reaction Network is a formalism that provides a way to model the interactions between entities. A reaction is an event that can occur in the system, which modifies each entity's quantities. Even if this formalism is originally used to describe chemical systems \citep{Feinberg2019}, it is also used in gene regulatory networks \citep{Karlebach2008} or systems of interactions between humans. An illustrative example that is a guideline model in the thesis is the SIR epidemiology model:
\begin{equation}
    \begin{aligned}
        Infection: S + I &\xrightarrow{k_i} 2 I \\
        Recovery: I &\xrightarrow{k_r} R
    \end{aligned}
\end{equation}
This Chemical Reaction Network has three types of entities (living beings in this context): {\em S} is the population that can be infected, {\em I} is the infected people, whereas {\em R} is the recovered/removed population. The system contains two reactions: either an infection or an immunisation/death can occur. 

Chemical Reaction Networks are algebraic models, but they induce mathematical models. Traditionally, their dynamics are described by a system of Ordinary Differential Equations (ODE). Such kind of dynamics is continuous and deterministic. If the deterministic model is relevant under some assumptions, such as a high enough species density, they are not adapted to populations of small sizes. With relaxed assumptions, the stochastic dynamics are described by a {\em Continuous-Time Markov Chain} (CTMC). The main property of such a stochastic process is the {\em memoryless property} (also known as Markov property): the system's future state only depends on the current state. For example, genetic networks have proven their stochastic nature \citep{Thattai2001}.

Statistical inference of CTMCs has been widely studied in the literature, particularly Bayesian statistics \citep{Craciun2013, Schnoerr2017, Loskot2019}. A general issue raised by such a complex stochastic model is the evaluation of the likelihood: it is intractable in most cases. 
The Bayesian framework allows the use of preexisting expertise on models via the prior distribution and quantifies the uncertainty of parameter inference with the posterior distribution. Also, it offers groups of likelihood-free methods, which allows approximating the posterior distribution. Approximate Bayesian Computation, a specific family of likelihood-free methods, is specifically studied in this thesis as our models have intractable likelihoods. It relies on Monte Carlo simulation methods: the general idea is to draw a parameter $\theta$, simulate the model $y \sim p(.|\theta)$, and keep the sample if $y$ is close enough to observations $\yexp$. Since its first appearance in the literature of population genetics \citep{Tavare1997,Pritchard1999} ABC methods have received much attention from the statistician community \citep{Marin2011} \citep{Sisson2018}. They have proven efficiency in the case of CTMCs \citep{Warne2019, Alharbi2018}.

Verification of a CTMC $\mcCTMC$ has been studied for about two decades since the appearance of CSL temporal logic \citep{Aziz1996}. As CTMCs are stochastic, verification of a CTMC considers the probability of verifying a specification $\varphi$. This kind of methods is divided into two approaches. First, the {\em qualitative approach} assesses whether the probability is above (or below) a {\em threshold}. Second, the {\em quantitative approach} is related to the {\em estimation} of such probabilities. Numerical methods have been developed for these purposes \citep{Baier2003}. To face the problem of state space explosion, statistical methods for verification have been considered since 2002 \citep{Legay2010,Legay2019}. This was enhanced by the growing interest of Systems Biology field in verification methods: the state-space of CTMCs defined by a Chemical Reaction Network quickly explodes or is infinite. 
Initially, these methods were developed for a single CTMC, but now verification methods tackle model checking with CTMCs indexed by parameters ({\em parametric CTMC}). The qualitative approach for parametric CTMC is called parameter synthesis. An approximate numerical method has been first proposed \citep{Han2008}, and also statistical methods \citep{SilvettiBortolussi2018}. For the quantitative approach, statistical methods have been developed \citep{Bortolussi2016}.

\section{Outline}

This thesis focuses on statistical inference and verification of parametric Continuous-Time Markov Chains defined by Chemical Reaction Networks. Our main methodological contribution relies on the formulation of a general algorithm called {\em automaton-ABC}, which uses HASL formalism within the ABC method. With this high-level algorithm, we address several tasks of statistical inference and statistical verification. The considered tools and problems are both issued from model checking and statistical inference, which positions this thesis at the frontier between these two fields. Efforts have been made to be intelligible for scientists of both fields. 
%The implementation of theses methods has lead to a Julia package.

The first three chapters are related to the literature.

\begin{itemize}
    \item Chapter~\ref{chap1} presents the basics of Markov Chains theory. The main properties of Continuous-Time Markov Chains (CTMC) are discussed. We define the probability measure over the set of {\em càdlàg} functions based on a CTMC. Then, Chemical Reaction Network formalism is defined. We see how a CTMC describes the stochastic dynamics, we discuss the other possible equations of the dynamics with additional assumptions, and we detail simulation algorithms for CTMC.

    \item Chapter~\ref{chap2} recalls concepts on statistical methods in inference with a particular focus on Monte Carlo simulations. First, some basic concept about statistical inference and Bayesian inference are discussed. Second, the main Monte Carlo methods are discussed. Third, we describe the Approximate Bayesian Computation (ABC) methods, detail several ABC samplers and discuss ABC applications' stakes. Finally, Kernel Density Estimation is presented.
    \item Chapter~\ref{chap3} discusses model checking for CTMCs. First, we detail two temporal logics: CSL and MITL. Then, verification of CTMCs are discussed (model checking for a single CTMC $\mcCTMC$, or parametric CTMC with a collection of CTMCs $\mcpCTMC$) with a particular focus on statistical methods. Finally, the HASL formalism is detailed.
\end{itemize}

The last two chapters expose contributions.

\begin{itemize}
    \item Chapter~\ref{chap4} details the statistical inference framework of CTMCs. We present the automaton-ABC algorithm, which combines the synchronised simulation offered by HASL formalism and ABC. We present two applications based on automaton-ABC: detection of oscillatory trends in parametric CTMC and faster implementation of classical ABC inference.
    \item Chapter~\ref{chap5} details a new statistical method based on automaton-ABC for the quantitative approach of model checking parametric CTMC in time-bounded reachability problems.
\end{itemize}

The new methods lead to a package in the cross-platform Julia language \citep{Bezanson2014}. The implementation is detailed in Appendix~\ref{appendix:pkg}.
Efforts have been made to make it easy to use, efficient and tested.
The Scripts List showed above in the thesis groups the scripts that mainly produce the results based on the package. The git repository of the thesis is available at:

\begin{center}
    \path{https://gitlab-research.centralesupelec.fr/2017bentrioum/phd-thesis}
\end{center}

A Notations List is available before the introduction. It groups the common notations used in this thesis.

\iffalse
Blabla un peu d'histoire
=> Introduction: covid19 pour introduire le pb de vérification et d'inférence
=> Ce n'est pas le seul domaine où il y a ces besoin: un peu de littérature.
=> Dans la thèse, on se focalise sur ces deux probèles dans le cadre d'une classe de moèdles des CRN. 
Parler des méthodes existantes. dire qu'on u un algo en contribution: automaton-ABC
=> Organisation de thèse: chap1, chap2 chap3...

Population asks "when the pandemic will be over" ? Does the 
Cependant, ce n'est pas la première fois qu'on a eu besoin de ces méthodes....
- La thèse relie vérification et méthodes statistiques.
\fi

