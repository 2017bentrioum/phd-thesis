
\iffalse

\subsubsection{An example with Poisson processes}

Let $\mathcal{M}_{\lambda}$ be the CTMC defined on a space set $\setS = \mathbb{N}$ by the initial distribution and the transition rate matrix $Q$ :
\begin{align*}
    \alpha &= \delta_0 \text{ (the initial state is 0)}\\
    Q(i, i+1) &= \lambda \text{ for } i \in \mathbb{N} \\
    Q(i, i) &= -\lambda \text{ for } i \in \mathbb{N} \\
    Q(i, j) &= 0 \text{ otherwise }
\end{align*}

What we've defined is a Poisson process of parameter $\lambda$. We can then consider a probability space $(\Omega, \mathcal{F},\Prob)$ and the equivalent stochastic process $\vaS_{\lambda} = (\vaS_{\lambda,t})_{t\geq0}$.

Let's solve the reachability problem :
\begin{equation*}
    \text{Estimate } \lambda \rightarrow \ProbMC[\lambda](\sigma \models \mathbf{F}_{[t_1,t_2]} (x_1 \leq \eltS \leq x_2))
\end{equation*}

The set $C_{\lambda,\varphi}$ is the set of trajectories that satisfies $\varphi = \mathbf{F}_{[t_1,t_2]} (x_1 \leq \eltS \leq x_2)$ for the CTMC ${\cal M}_{\lambda}$. There are powerful known results on Poisson processes as :
\begin{itemize}
    \item The trajectories are increasing step functions with jumps of value one (i.e has the form $1 \xrightarrow{t_1} 2 \xrightarrow{t_2} 3 \rightarrow \ldots$).
    \item $\forall t, v \in \Rgeq$ with  $v \leq t, \vaS_{\lambda,t} - \vaS_{\lambda,v} \sim \Poisson{\lambda(t-v)}$ (i.e. a Poisson distribution of parameter $\lambda(t-v)$. 
\end{itemize}

With the first above result, having one point in the eventual region $\mathbf{F}_{[t_1,t_2]} (x_1 \leq \eltS \leq x_2))$ is equivalent to have $\sigma@t_1 \leq x_2 \wedge \sigma@t_2 \geq x_1$. Indeed if $\sigma@t_1 > x_2$ then the trajectory is on the top of the region. If $\sigma@t_2 < x_1$ then the trajectory is below the region.

With this remark, $\ProbS[\lambda](C_{\varphi}) = \Prob(\vaS_{\lambda,t_1} \leq x_2, \vaS_{\lambda,t_2} \geq x_1)$.

\begin{flalign*}
    \begin{WithArrows}
        \Prob(\vaS_{\lambda,t_1} \leq x_2, \vaS_{\lambda,t_2} \geq x_1) &= \Prob(\vaS_{\lambda,t_2} \geq x_1 | \vaS_{\lambda,t_1} \leq x_2)\Prob(\vaS_{\lambda,t_1} \leq x_2) \Arrow{$\{\vaS_{\lambda,t_1} = k\}_{k=[[0,x_2]]}$\\ is a partition\\ of $\{\vaS_{\lambda,t_1} \leq x_2\}$} \\
                                                    &= \sum_{k=0}^{x_2} \Prob(\vaS_{\lambda,t_2} \geq x_1 | \vaS_{\lambda,t_1} = k) \Prob(\vaS_{\lambda,t_1} = k | \vaS_{\lambda,t_1} \leq x_2) \\
                                                    &= \sum_{k=0}^{x_2} \Prob(\vaS_{\lambda,t_2} - \vaS_{\lambda,t_1} \geq x_1 - k) \Prob(\vaS_{\lambda,t_1} = k) \\
                                                    &= \sum_{k=0}^{x_2} (1 - \Prob(\vaS_{\lambda,t_2} - \vaS_{\lambda,t_1} < x_1 - k)) \Prob(\vaS_{\lambda,t_1} = k) \\
                                                    &= \sum_{k=0}^{x_2} (1 - \Prob(\vaS_{\lambda,t_2} - \vaS_{\lambda,t_1} \leq x_1 - k -1)) \Prob(\vaS_{\lambda,t_1} = k) \\
    \end{WithArrows}
\end{flalign*}

As $\vaS_{\lambda,t_2} - \vaS_{\lambda,t_1} \sim \Poisson{\lambda(t_2-t_1))}$ and $\vaS_{\lambda,t_1} \sim \Poisson{\lambda t_1}$, $\ProbS[\lambda](C_{\lambda,\varphi})$ can be expressed analytically. As $\ProbS[\lambda] = \ProbMC[\lambda]$, the satisfaction probability function $\lambda \rightarrow \ProbMC[\lambda](\sigma \models \mathbf{F}_{[t_1,t_2]} (x_1 \leq \eltS \leq x_2))$ is expressed analytically. We have solved a time-bounded reachability problem analytically. 

The picture below is the run of the automaton-ABC algorithm with a prior $\pi(.) \sim \Unif{0}{30}$. In red, the estimated satisfaction probability function with gaussian kernel estimation of the ABC posterior. In blue, the true satisfaction probability function.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{pictures/poisson_abc.png}
    \caption{Histogram of the automaton-ABC posterior with $N = 200$ particles. In red : estimated satisfaction probability function; in blue : the analytical satisfaction probability function.}
    \label{fig:poisson_abc}
\end{figure}
\fi
\iffalse
\subsection{Link with the automaton-ABC algorithm}

We consider the below algorithm :

\begin{algorithm}[H]
    \begin{algorithmic}
        \Require $\pi(.)$ prior, $\mathcal{A}_{\varphi}$ automaton computes distance from MITL formula $\varphi$
        \Ensure $(\theta_i)_{1\leq i \leq N}$ drawn from $\pi_{\varphi-ABC}$
        \For {$i = 1:N$}
        \Repeat
        \State $\theta ' \sim \pi(.)$
        \State $\sigma' \sim \mathcal{M}_{\theta'}$
        \Until {$d(\sigma', \varphi) = 0$}
        \State $\theta_i, \sigma_i \gets \theta', \sigma'$
        \EndFor
    \end{algorithmic} 
    \caption{ABC driven by $\mathcal{A}_{\varphi}$ automaton} 
    \label{alg:abc_automaton}
\end{algorithm}

Then we have the following result :

\begin{thm}[Automaton-ABC posterior and satistfaction probability function]
    \begin{equation*}
        \pi_{\varphi-ABC}(\theta) = Pr(\varphi|\mathcal{M}_{\theta})\frac{\pi(\theta)}{K}
    \end{equation*}
\end{thm}

\begin{proof}

    \newcommand{\Cphi}{C^{\varphi}_{{\cal M}_{\theta}}}
    Let $\Cphi = \{\sigma \in \PathMC[\theta] / \sigma@0 \models \varphi \}$. Then $\ProbMC[\theta](\Cphi) = Pr(\varphi | \mathcal{M}_{\theta})$.

The samples $(\theta_i, \sigma_i)_{1\leq i \leq N}$ from the Algorithm~\ref{alg:abc_automaton} are drawn from a density $\pi_{\varphi-ABC}$ : 

    \begin{equation*}
        \pi_{\varphi-ABC}(\theta_i, \sigma_i) \propto \underbrace{\mathbb{1}_{d(.,\varphi) = 0}(\sigma_i)}_{\sigma_i \models \varphi} \underbrace{p_{{\cal M}_{\theta_i}}(\sigma_i)}_{\sigma_i \sim {\cal M}_{\theta_i}} \underbrace{\pi(\theta_i)}_{\text{prior}}  
    \end{equation*}
\noindent
    where $p_{{\cal M}_{\theta_i}}$ is the likelihood related to the CTMC ${\cal M}_{\theta_i}$.

    As $\sigma \in \Cphi \Leftrightarrow d(\sigma,\varphi) = 0$, $\mathbb{1}_{d(.,\varphi) = 0}(\sigma_i) = \mathbb{1}_{\Cphi}(\sigma_i)$. One can obtain the marginal distribution of $\theta$ by integration with the measure $\mubar$:
    \begin{align*}
        \begin{WithArrows}
            \pi_{\varphi-ABC}(\theta) &\propto \int_{\sigma \in \PathMC[\theta]} \mathbb{1}_{\Cphi}(\sigma) p_{{\cal M}_{\theta}}(\sigma) \pi(\theta) d\mubar \\ 
                                    &\propto \pi(\theta) \int_{\sigma \in \Cphi} p_{{\cal M}_{\theta}}(\sigma) d\mubar \Arrow{Corollary \ref{cor:likelihoodmc}}  \\
                                    &\propto \ProbMC[\theta](\Cphi) \pi(\theta) \\
    \end{WithArrows}
        \end{align*}

    As $\ProbMC[\theta](\Cphi) = Pr(\varphi|\mathcal{M}_{\theta})$, we can conlude that $\pi_{\varphi-ABC}(\theta) = Pr(\varphi|\mathcal{M}_{\theta})\frac{\pi(\theta)}{K}$. 
\end{proof}
\fi


