
\chapter{Conclusion}

Chemical Reaction Networks model a wide range of biological phenomena. If traditionally their dynamics are described by Ordinary Differential Equations, this approach is not relevant when the population is low. It is especially the case when one studies interactions between molecular species at the scale of the living cell (genetic regulatory networks, signalling pathways). By the reduction of such assumption, stochastic dynamics of CRN are described by CTMCs. 

This thesis addressed several tasks of statistical inference and verification for Continuous-Time Markov Chains described by Chemical Reaction Networks. 
%We apply our methods to several models from Systems Biology. 
We have endeavoured to prove the efficiency of the synchronised simulation of a CTMC with a Linear Hybrid Automaton within the ABC algorithm. This new method called automaton-ABC has been applied to several problems from Systems Biology.

First, we gave an overview of the literature for Bayesian statistical inference and statistical model checking for parametric CTMCs. We introduced the notations from both fields, detailed what makes the considered problems complex, and discussed state-of-the-art techniques.

Second, we used automaton-ABC for statistical inference of oscillatory genetic networks. The nature of observations concerns the oscillations. They are described by sentences such as "we observe $N$ oscillations between values $L$ and $H$ with mean period duration $\obsmeantp$". This qualitative behaviour is measured by an LHA $\aut_{per}$ \citep{Ballarini2015}, and we define a distance over such property of an oscillatory trajectory. The use of automaton-ABC over two oscillatory models (doping three-way oscillator and repressilator) allowed to show the parameter space subsets where specific kind of oscillations are the most probable.

Third, we used automaton-ABC to gain computational time in classical inference. HASL formalism offers a framework that tailors the simulation according to a specification. In this context, it was used to stop the simulation each time the Euclidean distance to observations exceeds the tolerance $\eps$ in the ABC procedure. In an experiment with the repressilator model, we showed that ABC computational time was divided by more than three compared to classical ABC.

Fourth, we used automaton-ABC for time-bounded reachability problems. The satisfiability distances represent the distance of a trajectory $\sigma$ from specific forms of MITL formula $\varphi$. We designed LHA to compute these distances and used automaton-ABC to address quantitative model checking for parametric CTMCs. More precisely, we highlighted the link between the satisfaction probability function $\spf$ and the automaton-ABC posterior with $\aut_{\varphi}$ named $\piphiabc$. Automaton-ABC-SMC allows exploring the parameter space concerning the formula $\varphi$ efficiently. The results were obtained over several models of CRN and constantly compared to either an analytical solution, model checking with Prism, or statistical model checking with Cosmos or Prism. Due to optimisation and parallelisation of our implementation,  the runs of automaton-ABC did not exceed two minutes, and outperformed state-of-the-art statistical model checking methods.

Finally, since these methods mix several concepts from both fields, we have tried to make their use accessible and efficient. We chose to use Julia because of its high-level syntax, efficiency, and great possibility of metaprogramming and code generation. It led to a Julia package that is much detailed in Appendix~\ref{appendix:pkg}. All the Julia scripts related to the results are documented within the Chapters, and the Scripts List above the introduction.

\section{Limits and Perspectives}

\subsection{Scope of our work}

This work focused on CTMCs defined by Chemical Reaction Networks. Indeed, they quickly challenge statistical inference or verification problems.
However, the methods are easily generalisable to generalised semi-Markov processes (which corresponds to Generalised Stochastic Petri Nets).

\subsection{Automaton-ABC for statistical inference}

In Section~\ref{sec:oscillatory_trends}, we gave a new approach to study oscillations in genetic networks whose stochastic dynamics are described by CTMCs. 
Another qualitative behaviour much observed in biological systems is bistability \citep{Tyson2008}. Bistable models are characterised by the existence of two steady states (and possibly unstable states). However, such stochastic models can produce trajectories that switch between two steady states. Thus, one can imagine an LHA that would characterises this behaviour, for example, by quantifying the time spent in a detected steady state or the duration time of a transition between two steady states, and use this LHA within the automaton-ABC procedure to identify the regions of the parameter space that reproduces this behaviour.

\subsection{Automaton-ABC for time-bounded reachability}

As already mentioned in Section~\ref{sec:lha_non_elementary}, the method only covers a fragment of MITL. Further work has to be done to cover all MITL formulae.
Instead of trying to design an automaton for each kind of formula, defining operations on HASL trajectory formulae could improve the coverage of MITL.

To illustrate this, we consider the computation of satisfiability distances of MITL formulae $\varphi_1 \land \varphi_2$ or $\varphi_1 \lor \varphi_2$.
We suppose $\varphi_1$ and $\varphi_2$ are formulae whose satisfiability distances are computed by already defined LHA $\aut_{\varphi_1}$ and $\aut_{\varphi_2}$ with HASL trajectory formulae $(Last(d_{\varphi_1}), \aut_{\varphi_1})$ and $(Last(d_{\varphi_2}), \aut_{\varphi_2})$. The idea is to define an LHA $\aut_{\varphi_1} \times \aut_{\varphi_2}$ that integrates the behaviours of $\aut_{\varphi_1}$ and $\aut_{\varphi_2}$ into one LHA.
%First, for $\varphi_1 \land \varphi_2$, an operation product $*$ could be defined to produce the HASL trajectory expression $Last(d_{\varphi_1}) + Last(d_{\varphi_2}), \aut_{\varphi_1} * \aut_{\varphi_2}$. An automaton $\aut_{\varphi_1} \times \aut_{\varphi_2}$ 
One can construct this automaton by defining a product over two LHA, whose location set is the cartesian product of $\aut_{\varphi_1}$ and $\aut_{\varphi_2}$; variables and flows are the concatenation of those from the two automata. For example, let us consider these two simple LHA:

\begin{figure}[H]
    \centering
    \begin{tabular}{cc}
        \begin{tikzpicture}[scale=1.0,every node/.style={scale=0.9,inner sep=1em},every path/.style={-triangle 90},initial text={}]
            % locations
            \node[state,rectangle,rounded corners=4pt] (l1) [initial] at (0,0) {$l_1$};
            \node[state,rectangle,rounded corners=4pt] (l2) at (3,0) {$l_2$};
            \draw (l1) -> node [midway,above] {$E,\gamma,U$} (l2);
        \end{tikzpicture}
    &
    \begin{tikzpicture}[scale=1.0,every node/.style={scale=0.9,inner sep=1em},every path/.style={-triangle 90},initial text={}]
        % locations
        \node[state,rectangle,rounded corners=4pt] (l1') [initial] at (0,0) {$l_1'$};
        \node[state,rectangle,rounded corners=4pt] (l2') at (3,0) {$l_2'$};
        \draw (l1') -> node [midway,above] {$E',\gamma',U'$} (l2');
    \end{tikzpicture}
    \end{tabular}
\end{figure}

Its product could be defined as:

\begin{figure}[H]
    \centering
    \begin{tikzpicture}[scale=1.0,every node/.style={scale=0.9,inner sep=1em},every path/.style={-triangle 90},initial text={\small begin}]
        % locations
        \node[state,rectangle,rounded corners=4pt] (l1l1') [initial] at (0,0) {$(l_1,l_1')$};
        \node[state,rectangle,rounded corners=4pt] (l1l2') at (5,0) {$(l_1,l_2')$};
        \node[state,rectangle,rounded corners=4pt] (l2l1') at (0,-5) {$(l_2,l_1')$};
        \node[state,rectangle,rounded corners=4pt] (l2l2') at (5,-5) {$(l_2,l_2')$};
        
        \draw (l1l1') -> node [midway,above,sloped] {$E,\gamma,U$} (l2l1');
        \draw (l1l2') -> node [midway,above,sloped] {$E,\gamma,U$} (l2l2');
        \draw (l1l1') -> node [midway,above,sloped] {$E',\gamma',U'$} (l1l2');
        \draw (l2l1') -> node [midway,above,sloped] {$E',\gamma',U'$} (l2l2');
        \draw (l1l1') -> node [midway,above,sloped] {$E\cap E',\gamma\land \gamma',U\cup U'$} (l2l2');
    \end{tikzpicture}
\end{figure}

With such operations, the distances from both formulae $\varphi_1$ and $\varphi_2$ would be computed even if simultaneous updates of the two automata variables occur. Indeed, $(l_1,l_1') \to (l_2,l_2')$ is fired if the occurred event is in $E\cap E'$ and the predicate $\gamma\land \gamma'$ is fulfilled. 
Thus, the satisfiability distance of $\varphi_1 \land \varphi_2$ could be $(Last(d_{\varphi_1} + d_{\varphi_2}), \aut_{\varphi_1} \times \aut_{\varphi_2})$.
For $\varphi_1 \lor \varphi_2$, the satisfiability distance could be $(Last(d_{\varphi_1} * d_{\varphi_2}), \aut_{\varphi_1} \times \aut_{\varphi_2})$.

As \citep{SilvettiBortolussi2018} did with \citep{Bortolussi2016}, a statistical parameter synthesis method may be constructed on top of automaton-ABC for time-bounded reachability. The stake is to statistically control the approximation of kernel density estimator based on automaton-ABC samples. For example, one can relax the tolerance by setting $\eps > 0$ to have better confidence for parameters that do not verify a formula $\varphi$.

In complex CTMC models, exact simulation has a high computational cost. One can use approximate simulation methods such as Tauleaping (Algorithm~\ref{alg:tau_leap}) to reduce the computational cost. The smaller the time resolution $\tau$, the better the simulation approximation. The Multi-Level ABC method \citep{Lester2018}, evoked in Section~\ref{subsec:time_obs}, could be a solution to improve the performance of our automaton-ABC procedure.

\subsection{Implementation}

In our implementation detailed in Appendix~\ref{appendix:pkg}, defining a CTMC by a CRN and apply ABC-SMC and automaton-ABC-SMC is easy with the LHA presented in this thesis. However, further work will be performed to improve our tool:
\begin{itemize}
    \item It is possible but challenging (without help) to design its own LHA. Thus, work is in progress to render the creation of LHA as simple as CTMCs in the package.
    \item Our implementation does not cover the whole HASL expression grammar, which would be interesting for more sophisticated runs of automaton-ABC.
\end{itemize}

\section{Last words}

Many phenomena in Systems Biology involve complex behaviours that are stochastic and complex.
Performing inference or verification on such models is generally difficult. In this thesis, we attempted to show how Bayesian inference and statistical verification, via the Approximate Bayesian Computation (ABC) methods and the Hybrid Automata Stochastic Language (HASL), could both benefit from each other. Bayesian inference has benefited from HASL by its capacity of formally describe such behaviours. Statistical verification has benefited from Bayesian inference by its efficiency to explore the parameter space of statistical models.


%\begin{qu}
%    Milios
%\end{qu}

\iffalse
Perspectives:
- Etudier la vérification de propriété avec des trajectoires approximées (ML ABC)
- Automate périodique: done
- Opération * sur les LHA
- Copulas pour KDE
- Inférence Milios & ABC
- Généralisable aux GSPN: on a parlé que CTMC car le formalisme choisit était CRN
- Nouvel sur-automate produit
\fi

% Bootstrap du sample ABC et KDE

