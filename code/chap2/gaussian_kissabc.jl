
using Random
using Distributions
using KernelDensity
using KissABC
using Plots

Random.seed!(6)

sim(μ) = randn(50) .+ μ
data = sim(0.0)
prior = Uniform(-1,1)
cost(μ) = abs(mean(sim(μ)) - mean(data))

function pdf_true_abc_post(μ, ϵ)  
    gaussian_abc_th = Normal(θ, 1/sqrt(n_obs))
    return (cdf(gaussian_abc_th, mean_obs+ϵ) - cdf(gaussian_abc_th, mean_obs-ϵ))/(2*ϵ)
end

pdf_true_post(μ) = pdf(Normal(mean(data), 1/sqrt(50)), μ)

for ϵ in [0.1, 0.2]
    for nbr_pa in [1000, 10000]
        ressmc = smc(prior, cost, nparticles=100, epstol=ϵ)
        μ_post = ressmc[1].particles
        estim_abc_post = kde(μ_post)
        histogram(μ_post, normalize = :density)
        savefig("abc_gauss_$(nbr_pa)_$(ϵ).svg")
    end
end

