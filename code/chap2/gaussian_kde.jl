
using Plots
using Distributions
using BoundedKDE

path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : ""
path_pic = path * "/pictures/chap2/"

# Observations: 0.0, 1.0, 1.0, 3.0
observations = [0.0, 1.0, 1.0, 3.0]
pdf_obs1(x) = pdf(Normal(0.0), x)
pdf_obs2(x) = pdf(Normal(1.0), x)
pdf_obs3(x) = pdf(Normal(3.0), x)

#kde(x) = (1/4)*(pdf_obs1(x) + 2*pdf_obs2(x) + pdf_obs3(x))
dist_kde = UnivariateKDE(observations; bandwidth = 1.0, kernel = "gaussian")
kde(x) = pdf(dist_kde, x)

p = plot(title = "Gaussian KDE with observations [0.0, 1.0, 1.0, 3.0] \nand bandwidth 1.0", dpi = 480)
scatter!(p, [0.0, 1.0, 3.0], zeros(3), color = "blue", label = "")
scatter!(p, [1.0], [0.01], color = "blue", label = "observations")
xaxis = -2:0.1:6
plot!(p, xaxis, pdf_obs1.(xaxis), color = "red", label = "")
plot!(p, xaxis, pdf_obs2.(xaxis), color = "red", label = "")
plot!(p, xaxis, pdf_obs3.(xaxis), color = "red", label = "gaussian density")
plot!(p, xaxis, kde.(xaxis), color = "green", label = "KDE")
savefig(path_pic * "gaussian_kde.svg")

