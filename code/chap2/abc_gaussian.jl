
using Distributed
using HypothesisTests
import Distributions: cdf, pdf, ContinuousUnivariateDistribution
import SpecialFunctions: erf
import Statistics: var
import QuadGK: quadgk
@everywhere using pygmalion
@everywhere using Random
using DelimitedFiles

function redirect_to_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end


include(ENV["PYGMALION_DIR"] * "etc/plot.jl")
include(ENV["PYGMALION_DIR"] * "algorithms/estimation/abc_pop_mc.jl")

@everywhere str_m = "gaussian"
str_d = "abc_gaussian"
str_dir =  create_results_directory(; title = str_d)
@everywhere load_model(str_m)

str_om = "x"
ll_om = ["x"]
tml = 1:50 # timeline for the simulation (100 steps here)
g = create_observation_function([ObserverModel(str_om, collect(tml))]) # observation function with observation noise
mn = nothing
on = nothing
cfg_x0 = State(0.0)
cfg_p_true = Parameters(0.0, 1.0)
u = Control()
cfg_so_exp = simulate(f, g, cfg_x0, u, cfg_p_true; mn = mn, on = on, full_timeline = true)
cfg_so_exp = load_from_obs(ENV["PYGMALION_DIR"] * "database/observations/gaussian/mu_50.csv")
cfg_ll_p = ["mu"]

@everywhere myseed = 2121
@everywhere (VERSION >= v"0.7.0") ? Random.seed!(myseed) : srand(myseed)

# Configuration of priors
cfg_srl_p = [SamplingRule(["mu"], "uniform", (-1.0, 1.0))]
#SamplingRule(["std"], "uniform", (0.1, 2.0))]
cfg_bl_p = [Bound("mu", -Inf, Inf)]
# Bound("std", 0.0, Inf)]

@everywhere function mean_ss(so::Union{SystemObservation,Nothing})
    if so == nothing return Inf end
    res = 0.0
    l_obs = so.otll[1]
    n_obs = length(l_obs)
    for i = 1:n_obs
        res += l_obs[i][2][1]
    end
    return [res / n_obs]
end

mean_obs = mean_ss(cfg_so_exp)[1]
@show mean(to_vec(cfg_so_exp, "x"))

## configuration abc
cfg_nbr_particles = 100
cfg_eta_function = mean_ss
cfg_str_distance_function = "euclidean_distance"
cfg_str_statistical_measure = "mean"
cfg_kernel_type = "mv_normal"
cfg_alpha = 0.75
prct_eps = [0.1]
cfg_l_epsilon = [prct * sqrt(dot(mean_ss(cfg_so_exp), mean_ss(cfg_so_exp))) for prct in prct_eps]
cfg_l_epsilon = [cfg_l_epsilon[end]]
cfg_l_epsilon = [1.0]

r_abc_pmc = nothing
p_abc = nothing
so_abc = nothing


#redirect_to_files(str_dir * "abc_pmc.log", str_dir * "abc_pmc.err") do
    cfg_abc = ConfigurationAbcPopMc(cfg_ll_p, cfg_srl_p, cfg_bl_p, cfg_nbr_particles, cfg_eta_function, 
                                    cfg_str_distance_function, cfg_str_statistical_measure, cfg_l_epsilon, cfg_alpha, cfg_kernel_type)
    println("ABC simple")
    @timev global r_abc_pmc = abc_pop_mc(f, g, cfg_x0, u, cfg_p_true, mn, on, cfg_so_exp, cfg_abc)
    @show r_abc_pmc.nbr_sim
    
    mean_post = 0.0
    for i = 1:cfg_nbr_particles
        global mean_post += r_abc_pmc.weights[i] * r_abc_pmc.mat_p_end[1,i]
    end
    err_mean = round(abs(mean_post - mean_obs), digits=6)
    fig = plt.figure()
    plt.title("eps = $(r_abc_pmc.epsilon), mean_obs = $(round(mean_obs, digits=6))\n err_mean = $err_mean")
    plt.hist(r_abc_pmc.mat_p_end[1,:], weights = r_abc_pmc.weights, bins = 20)
    plt.savefig(str_dir * "hist_param.png", dpi=480)
    plt.close()
#end

function pdf_abc_post(θ::Real)  
    gaussian_abc_th = Normal(θ, 1/sqrt(n_obs))
    return (cdf(gaussian_abc_th, mean_obs+eps) - cdf(gaussian_abc_th, mean_obs-eps))/(2*eps)
end
pdf_post(θ::Real) = Distributions.pdf(Normal(mean_obs, 1/sqrt(n_obs)), θ)
eps = cfg_l_epsilon[end]
n_obs = 50
var_th_abc_post = ((cfg_p_true.std^2)/n_obs + (eps^2)/3)
int_var = quadgk(θ -> θ^2 * pdf_abc_post(θ), -Inf, Inf, rtol=1e-10)[1]
@show int_var
@show (int_var + mean_obs^2)
@show var_th_abc_post, var(r_abc_pmc.mat_p_end[1,:])
x = -2:0.02:2

struct AbcPostNormal <: ContinuousUnivariateDistribution
    mean_obs::Float64
    n_obs::Int
    std_post::Float64
    eps::Float64
end
Distributions.length(d::AbcPostNormal) = 1
Distributions.sampler(d::AbcPostNormal) = d
# rand
# quantile
function Distributions.pdf(d::AbcPostNormal, x::Real)
    gaussian_abc_th = Normal(x, d.std_post/sqrt(d.n_obs))
    return (cdf(gaussian_abc_th, d.mean_obs+d.eps) - cdf(gaussian_abc_th, d.mean_obs-d.eps))/(2*d.eps)
end
function Distributions.logpdf(d::AbcPostNormal, x::Real)
    gaussian_abc_th = Normal(x, d.std_post/sqrt(d.n_obs))
    return log((cdf(gaussian_abc_th, d.mean_obs+d.eps) - cdf(gaussian_abc_th, d.mean_obs-d.eps))/(2*d.eps))
end
function Distributions.cdf(d::AbcPostNormal, x::Real)
    int_cdf = quadgk(ybar -> 0.5*(1+erf(sqrt(d.n_obs)*(x-ybar)/(d.std_post*sqrt(2)))), 
                                     d.mean_obs-d.eps, d.mean_obs+d.eps, rtol=1e-8)[1]
    return int_cdf / (2*d.eps)
end
Distributions.mean(d::AbcPostNormal) = d.mean_obs
Distributions.var(d::AbcPostNormal) = d.std_post^2/d.n_obs + d.eps^2/3
Distributions.maximum(d::AbcPostNormal) = Inf
Distributions.minimum(d::AbcPostNormal) = -Inf
eltype(d::AbcPostNormal) = Float64
Distributions.insupport(d::AbcPostNormal, x::Real) = true


y_abc_th = pdf_abc_post.(x)
y_post_th = pdf_post.(x)
y_lim = maximum(vcat(y_abc_th, y_post_th))*1.01

fig, ax1 = plt.subplots()
c = round(0.95 * err_mean, digits=5)
pr_err_c = (var_th_abc_post*sum(r_abc_pmc.weights.^2)) / c^2
plt.title("eps = $eps \n mean_obs = $(round(mean_obs, digits=6)), mean_abc_samples = $(round(mean_post, digits=6))\n 
          err_mean = $err_mean pr(err_mean > $c) <= $(round(pr_err_c, digits=4))")
col_hist = "grey"
ax1.set_xlabel("mu obs")
ax1.set_ylabel("Histogram ABC posterior", color = col_hist)
ax1.hist(r_abc_pmc.mat_p_end[1,:], bins = 25, color = col_hist, weights = r_abc_pmc.weights)
ax1.tick_params(axis="y", labelcolor = col_hist)
ax2 = ax1.twinx()
col_plot = "blue"
ax2.set_ylim(bottom=0.0, top=y_lim)
ax2.plot(x, y_abc_th, "--", label = "true abc post", color = col_plot, linewidth = 1.0, ms = 1.0)
ax2 = ax1.twinx()
col_plot = "green"
ax2.set_ylim(bottom=0.0, top=y_lim)
ax2.plot(x, y_post_th, "--", label = "true post", color = col_plot, linewidth = 1.0, ms = 1.0)
fig.tight_layout()
fig.legend(loc = "upper right", bbox_to_anchor = (0.9, 0.8))
plt.savefig(str_dir * "hist2.png", dpi=480)
plt.close()


## Unif
d_w = Categorical(r_abc_pmc.weights)
unif_abc_post = r_abc_pmc.mat_p_end[1,rand(d_w, cfg_nbr_particles)] 

d_abc_post = AbcPostNormal(mean_obs, n_obs, cfg_p_true.std, eps)
ks_test_unif = ExactOneSampleKSTest(unif_abc_post, d_abc_post)
ks_test_weighted = ExactOneSampleKSTest(r_abc_pmc.mat_p_end[1,:], d_abc_post)
@show ks_test_unif
@show ks_test_weighted

fig, ax1 = plt.subplots()
c = round(0.95 * err_mean, digits=5)
pr_err_c = (var_th_abc_post) / (cfg_nbr_particles * c^2)
plt.title("eps = $eps \n mean_obs = $(round(mean_obs, digits=6)), mean_abc_samples = $(round(mean_post, digits=6))\n 
          err_mean = $err_mean pr(err_mean > $c) <= $(round(pr_err_c, digits=4))")
col_hist = "grey"
ax1.set_xlabel("mu obs")
ax1.set_ylabel("Histogram ABC unif posterior", color = col_hist)
ax1.hist(unif_abc_post, bins = 25, color = col_hist)
ax1.tick_params(axis="y", labelcolor = col_hist)
ax2 = ax1.twinx()
col_plot = "blue"
ax2.set_ylim(bottom=0.0, top=y_lim)
ax2.plot(x, y_abc_th, "--", label = "true abc post", color = col_plot, linewidth = 1.0, ms = 1.0)
ax2 = ax1.twinx()
col_plot = "green"
ax2.set_ylim(bottom=0.0, top=y_lim)
ax2.plot(x, y_post_th, "--", label = "true post", color = col_plot, linewidth = 1.0, ms = 1.0)
fig.tight_layout()
fig.legend(loc = "upper right", bbox_to_anchor = (0.9, 0.8))
plt.savefig(str_dir * "hist2_unif.png", dpi=480)
plt.close()

d_abc_post = AbcPostNormal(mean_obs, n_obs, cfg_p_true.std, 0.2)
d_post = Normal(mean_obs, cfg_p_true.std/sqrt(n_obs))
cdf_post(x) = cdf(d_post, x)
cdf_abc_post(x) = cdf(d_abc_post, x)

x_cdf = -5:0.02:5

plt.figure()
plt.plot(x_cdf, cdf_abc_post.(x_cdf), "ro--", linewidth=1.0, ms=1.0, label="true abc post cdf")
plt.plot(x_cdf, cdf_post.(x_cdf), "bo--", linewidth=1.0, ms=1.0, label="true post cdf")
fig.legend(loc = "upper right", bbox_to_anchor = (0.9, 0.8))
plt.savefig(str_dir * "cdf_post_abc_post.png", dpi=480)
plt.close()

d_abc_post = AbcPostNormal(mean_obs, n_obs, cfg_p_true.std, 0.2)
d_post = Normal(mean_obs, cfg_p_true.std/sqrt(n_obs))
d_pdf_post(x) = pdf(d_post, x)
d_pdf_abc_post(x) = pdf(d_abc_post, x)

plt.figure()
plt.plot(x_cdf, d_pdf_abc_post.(x_cdf), "ro--", linewidth=1.0, ms=1.0, label="true abc post pdf")
plt.plot(x_cdf, d_pdf_post.(x_cdf), "bo--", linewidth=1.0, ms=1.0, label="true post pdf")
fig.legend(loc = "upper right", bbox_to_anchor = (0.9, 0.8))
plt.savefig(str_dir * "pdf_post_abc_post.png", dpi=480)
plt.close()


