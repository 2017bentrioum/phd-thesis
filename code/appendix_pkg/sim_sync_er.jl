
using MarkovProcesses
load_plots()
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : ""

load_model("ER")
load_automaton("automaton_F")
A_F = create_automaton_F(ER, 50.0, 75.0, 0.025, 0.05, :P)
sync_ER = A_F * ER
σ = simulate(sync_ER)
plot(σ; A = A_F, filename=path*"/pictures/appendix_pkg/sim_sync_er.svg")


