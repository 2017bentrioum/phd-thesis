
using MarkovProcesses
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : ""

load_plots()
load_model("SIR")
σ = simulate(SIR)
plot(σ; filename=path*"/pictures/appendix_pkg/sim_sir.svg")

