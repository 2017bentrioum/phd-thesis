
using MarkovProcesses
using Plots
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : ""

load_model("ER")
load_automaton("automaton_F")
A_F_R1 = create_automaton_F(ER, 50.0, 75.0, 0.025, 0.05, :P)
sync_ER = A_F_R1*ER 

parametric_sync_ER = ParametricModel(sync_ER, (:k3, Uniform(0.0, 100.0)))
res = automaton_abc(parametric_sync_ER; nbr_particles = 1000)

histogram(res.mat_p_end', weights = res.weights, normalize = :density)
savefig(path * "/pictures/appendix_pkg/R1_hist.svg")

