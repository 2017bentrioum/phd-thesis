
function get_value(name_file::String, field::String) 
    output_file = open(name_file)
    str_val = missing
    val = missing
    while !eof(output_file)
        line = readline(output_file)
        splitted_line = split(line, ':')
        if (length(splitted_line) > 1) && (splitted_line[1] == "Estimated value")
            str_val = splitted_line[2]
            break
        end
    end
    close(output_file)
    if !ismissing(str_val)
        str_val = split(str_val, '\t')[2]
        val = parse(Float64, str_val)
    end
    return val
end

cosmos_get_value(name_file::String) = get_value(name_file, "Estimated value")
prism_get_value(name_file::String) = get_value(name_file, "Result")

