
@everywhere using MarkovProcesses
using DelimitedFiles

path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_csv = (path_absolute != ".") ? path_absolute * "/code/chap5/julia/estim_MC/viral_cn_ca" : "."
if !isdir(path_csv) mkdir(path_csv) end
include(path_absolute * "/code/chap5/parser_res.jl")

load_model("intracellular_viral_infection")
load_automaton("automaton_G_and_F")
aut = create_automaton_G_and_F(intracellular_viral_infection, 0.0, 10.0, 0.0, 50.0, :G, 101.0, Inf, 50.0, 200.0, :G)
sync_viral = aut * intracellular_viral_infection 

l_cn = 0.6:0.1:1.1
l_ca = 0.5:0.15:2.0
nb_cn = length(l_cn)
nb_ca = length(l_ca)
mat_cn = zeros(nb_cn, nb_ca)
mat_ca = zeros(nb_cn, nb_ca)
mat_prob = zeros(nb_cn, nb_ca)
time_begin = time()
for i = eachindex(l_cn)
    for j = eachindex(l_ca)
        cn, ca = l_cn[i], l_ca[j]
        mat_cn[i,j], mat_ca[i,j] = cn, ca
        set_param!(sync_viral, [:cn, :ca], [cn, ca])
        mat_prob[i,j] = smc_chernoff(sync_viral; approx = 0.01, confidence = 0.99)
    end
end
time_end = time()

writedlm(path_csv * "/grid_X_smc.csv", mat_cn, ',')
writedlm(path_csv * "/grid_Y_smc.csv", mat_ca, ',')
writedlm(path_csv * "/satisfaction_func_smc.csv", mat_prob, ',')

file_out = open(path_csv * "/stdout_smc.out", "w")
write(file_out, "Time : $(time_end - time_begin)\n")
close(file_out)

