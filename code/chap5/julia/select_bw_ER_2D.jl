
using DelimitedFiles
using Distributed
@everywhere using MarkovProcesses
using Dates
using Plots

@assert length(ARGS) > 0 "No arguments entered. Please specify at least one argument that specifies the experiment: R4, R5, R6; e.g. julia enzym_2d.jl R5."
@assert length(ARGS) <= 2 "Too much arguments"
exp = ARGS[1]
@assert exp in ["R4", "R5", "R6"] "Wrong name of experiment"
nbr_exp = parse(Int, exp[2]) - 3
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path *= "/code/chap5/julia"
if length(ARGS) == 2
    path_results = ARGS[2]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end
exps_p_star_k1_k2 = [[0.005, 95.0], [0.25, 40.0], [0.15, 10.0]]
exps_prob_p_star_k1_k2 = [0.573094, 1.0, 0.522753]

samples_abc_post = readdlm(ENV["BENTRIOU_THESIS"] * "/pictures/chap5/automaton_abc/results_$(exp)/mat_p_end.csv", ',')
samples_weights = readdlm(ENV["BENTRIOU_THESIS"] * "/pictures/chap5/automaton_abc/results_$(exp)/weights_end.csv", ',')[:,1]

# Histogram
histogram2d(samples_abc_post[1,:], samples_abc_post[2,:], weights = samples_weights, 
            xtickfontsize = 16, ytickfontsize = 16,
            xtickfonthalign = :right,
            tickfonthalign = :right,
            tickfontvalign = :top,
            ytickfonthalign = :top,
            right_margin = 15Plots.mm,
            normalize = :density)
savefig(path_results * "/histogram.svg")

## Satisfaction function

# Optimal bandwidth with BoundedKDE
time_begin_kde = time()
using BoundedKDE
d_exp_kernel = Dict("R4" => "gaussian",
                    "R5" => "chen99",
                    "R6" => "chen99")
kernel_kde = d_exp_kernel[exp]
density_ubound = (exp == "R4") ? [0.05, 100.0] : [2.0, 100.0]
nbr_points_axis = 3
bw_lbound = [[0.5, 0.5], [0.5, 0.5], [0.5, 0.5]]
bw_ubound = [[2.0, 2.0], [2.0, 2.0], [2.0, 2.0]]
estim_abc_post = MultivariateKDE(samples_abc_post; kernel = kernel_kde, weights = samples_weights, lower_bound = [0.0, 0.0], upper_bound = density_ubound)
bw_ref = asymptotic_bandwidth(estim_abc_post)
#@show kernel_kde
lscv_bandwidth = select_bandwidth(estim_abc_post, bw_lbound[nbr_exp], bw_ubound[nbr_exp], fill(nbr_points_axis, 2); maxevals_int = typemax(Int))
#lscv_bandwidth = minimize_lscv(estim_abc_post, bw_lbound[nbr_exp], bw_ubound[nbr_exp]; 
#                               #coeff_init_bw = coeff_bw_R5, 
#                               rt = 0.5, f_tol = 1E-5, x_tol = 1E-4,
#                               verbosity = 2, maxevals_int = typemax(Int))
@show lscv_bandwidth, lscv_bandwidth / asymptotic_bandwidth(estim_abc_post)
estim_abc_post = change_bandwidth(estim_abc_post, lscv_bandwidth)
pdf_estim_abc_post(x) = pdf(estim_abc_post, x)
time_end_kde = time()

# Estimation of the constant with a probability estimated by Statistical Model Checking
constant = exps_prob_p_star_k1_k2[nbr_exp] / pdf_estim_abc_post(exps_p_star_k1_k2[nbr_exp])
prob_func(x,y) = pdf_estim_abc_post([x,y]) * constant

file_out = open(path_results * "/std.out", "w")
write(file_out, "Kernel: $(kernel_kde)\n")
write(file_out, "Gridsearch of bandwidth (coefficients): [$(bw_lbound[nbr_exp][1]),$(bw_ubound[nbr_exp][1])] x [$(bw_lbound[nbr_exp][2]),$(bw_ubound[nbr_exp][2])] with $nbr_points_axis steps\n")
write(file_out, "LSCV minimum bandwidth: $(lscv_bandwidth)\n")
write(file_out, "Coefficient bandwidth of minimum LSCV: $(lscv_bandwidth / asymptotic_bandwidth(estim_abc_post))\n")
write(file_out, "Constant: $(constant)\n")
write(file_out, "Time: $(time_end_kde - time_begin_kde)\n")
close(file_out)

# Plot of satisfaction probability function
@show prob_func(exps_p_star_k1_k2[nbr_exp]...), exps_prob_p_star_k1_k2[nbr_exp]
xaxis = (exp == "R4") ? (0:0.0005:0.05) : (0:0.015:1.5)
yaxis = 0:1.0:100.0
p = plot(title = "Multivariate KDE", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
plot!(p, xaxis, yaxis, prob_func, st = :surface, c = :coolwarm, camera = (30, 45))
savefig(path_results * "/estim_kde_satisfaction_prob_function.svg")

