
using Distributed
@everywhere using MarkovProcesses
using Dates
using Plots
using DelimitedFiles
using Distributions

@assert length(ARGS) <= 1 "Too much arguments"
exp = "poisson"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path *= "/code/chap5/julia"
if length(ARGS) == 1
    path_results = ARGS[1]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end

# Chemical reaction network model
load_model("poisson")
# Choice of the automaton
load_automaton("automaton_F")
aut = create_automaton_F(poisson,  5.0, 7.0, 0.75, 1.0, :N)
# Synchronized model
sync_poisson = aut * poisson 
pm_sync_poisson = ParametricModel(sync_poisson, (:λ, Uniform(0.0, 30.0)))
nbr_pa = 500
α = 0.5

r = automaton_abc(pm_sync_poisson; nbr_particles = nbr_pa, alpha = α, dir_results = path_results)
samples_abc_post = r.mat_p_end[1,:]
samples_weights = r.weights

# Histogram
histogram(samples_abc_post, weights = r.weights, normalize = :density)
savefig(path_results * "/histogram.svg")

function true_pr_func(λ::Float64, A::LHA)
    val = 0.0
    N_t1 = Poisson(λ*A.constants[:t1])
    N_t2 = Poisson(λ*A.constants[:t2])
    N_t2mt1 = Poisson(λ*(A.constants[:t2]-A.constants[:t1]))
    for i = 0:A.constants[:x2]
        val += (1-cdf(N_t2mt1, A.constants[:x1]-i-1))*pdf(N_t1, i)
    end
    return val
end
true_pr_func(λ::Float64) = true_pr_func(λ, aut)

# Optimal bandwidth with BoundedKDE
time_begin_kde = time()
using BoundedKDE
kernel_kde = "gaussian"
bw_lbound = 0.05
bw_ubound = 5.0
estim_abc_post = UnivariateKDE(samples_abc_post; kernel = kernel_kde, weights = r.weights, lower_bound = 0.0, upper_bound = 30.0)
lscv_bandwidth = minimize_lscv(estim_abc_post, bw_lbound, bw_ubound; method = :goldensection, verbose = true, rel_tol = 1e-8)
estim_abc_post = change_bandwidth(estim_abc_post, lscv_bandwidth)
pdf_estim_abc_post_pkg(x) = pdf(estim_abc_post, x)

# Estimation of the constant with a probability estimated by Statistical Model Checking
compute_cte(λ::Float64) = true_pr_func(λ) / pdf_estim_abc_post_pkg(λ)
constant_pkg = mean(compute_cte.([7.0, 7.5]))
#=
constant_2 = compute_cte(12.0)
constant_3 = mean(compute_cte.([6.0, 10.0, 12.0]))
constant_4 = mean(compute_cte.([7.5, 8.0, 8.5]))
=#
# Satisfaction probability function
prob_func_pkg(x) = pdf_estim_abc_post_pkg(x) * constant_pkg
#=
prob_func_2(x) = pdf_estim_abc_post_pkg(x) * constant_2
prob_func_3(x) = pdf_estim_abc_post_pkg(x) * constant_3
prob_func_4(x) = pdf_estim_abc_post_pkg(x) * constant_4
=#
time_end_kde = time()

# Plot of satisfaction probability function
xaxis = 0.0:0.3:30.0
plot(title = "Satisfaction probability for poisson process", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
#plot!(xaxis, prob_func.(xaxis), label = "Estimated spf")
plot!(xaxis, prob_func_pkg.(xaxis), label = "Estimated spf")
plot!(xaxis, true_pr_func, label = "Analytical spf")
savefig(path_results * "/estim_kde_satisfaction_prob_function.svg")

file_out = open(path_results * "/std.out", "w")
write(file_out, "Kernel: $(kernel_kde)\n")
write(file_out, "LSCV bandwidth: $(lscv_bandwidth)\n")
write(file_out, "Coefft LSCV bandwidth: $(lscv_bandwidth / asymptotic_bandwidth(estim_abc_post))\n")
write(file_out, "Constant: $(constant_pkg))\n")
write(file_out, "Time: $(time_end_kde - time_begin_kde))\n")
close(file_out)


