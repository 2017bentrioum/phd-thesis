
using MarkovProcesses
using Plots

path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_pics = path * "/pictures/chap5/"

load_plots()
load_model("ER")
set_time_bound!(ER, 0.1)
set_param!(ER, [:k1, :k2], [1.0, 1.0])

load_automaton("automaton_F")
A_F_R1 = create_automaton_F(ER, 50.0, 75.0, 0.025, 0.05, :P) 
A_F_R2 = create_automaton_F(ER, 50.0, 75.0, 0.05, 0.075, :P) 
A_F_R3 = create_automaton_F(ER, 25.0, 50.0, 0.05, 0.075, :P)

list_k3 = [10.0, 20.0, 50.0]
list_colors = [:green, :blue, :red]

p = plot(title = "Trajectories of ER model", palette = :lightrainbow, legend = :outertopright, background_color_legend=:transparent, dpi = 480)
for i = eachindex(list_k3)
    k3, color = list_k3[i], list_colors[i]
    set_param!(ER, :k3, k3)
    σ = simulate(ER)
    plot!(times(σ), σ.P, linetype = :steppost, color = color, xlabel = "Time", ylabel = "Number of molecules of P", label = "k3 = $(convert(Int, k3))")
    for i = 1:4
        σ = simulate(ER)
        plot!(times(σ), σ.P, linetype = :steppost, color = color, label = "")
    end
end
plot!(A_F_R1, label = "TR1")
plot!(A_F_R2, label = "TR2")
plot!(A_F_R3, label = "TR3")

savefig(path_pics * "regions_er_1d.svg")

