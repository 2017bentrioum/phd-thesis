
using MarkovProcesses

path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_pics = path * "/pictures/chap5/"

load_plots()
load_model("ER")
observe_all!(ER)
set_time_bound!(ER, Inf)

set_param!(ER, [:k1, :k2, :k3], [1.0, 1.0, 1.0])
σ = simulate(ER)
plot(σ; filename = path_pics * "sim_er_k3_1.svg")

set_param!(ER, [:k1, :k2, :k3], [1.0, 1.0, 0.1])
σ = simulate(ER)
plot(σ; filename = path_pics * "sim_er_k3_0_1.svg")

