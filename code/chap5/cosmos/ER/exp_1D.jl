
using Plots
import DelimitedFiles: writedlm
using Dates
using Distributed
path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
include(path_absolute * "/code/chap5/parser_res.jl")

@assert length(ARGS) > 0 "No arguments entered. Please specify at least one argument that specifies the experiment: R1, R2, R3; e.g. julia exp_1d.jl R3."
@assert length(ARGS) <= 2 "Too much arguments"
exp = ARGS[1]
@assert exp in ["R1", "R2", "R3"] "Wrong name of experiment"
nbr_exp = parse(Int, exp[2])
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path_cosmos_files = path_absolute * "/code/chap5/cosmos/ER"
if length(ARGS) == 2
    path_results = ARGS[2]
else
    path_results = path_cosmos_files * "/results_$(exp)_$(date_run)"
end
if !isdir(path_results) mkdir(path_results) end
if !isdir(path_results * "/pics") mkdir(path_results * "/pics") end
if !isdir(path_results * "/csv") mkdir(path_results * "/csv") end
# Values x1, x2  t1, t2
dict_exp = Dict(
                "R1" => [50, 75, 0.025, 0.05],
                "R2" => [50, 75, 0.05, 0.075],
                "R3" => [25, 50, 0.05, 0.075]
               )
nb_jobs = haskey(ENV, "SLURM_NTASKS") ? ENV["SLURM_NTASKS"] : nworkers()
width = 0.1
level = 0.99
val_exp = dict_exp[exp]
x1, x2, t1, t2 = val_exp[1], val_exp[2], val_exp[3], val_exp[4]
l_k3 = 0:10:100
nb_param = length(l_k3)
l_dist = zeros(nb_param)
time_begin_cosmos = time()
for i in 1:nb_param
    k3 = l_k3[i]
    cmd = `Cosmos $(path_cosmos_files * "/model.gspn") $(path_cosmos_files * "/dist_F.lha") --njob $(nb_jobs) 
    --const k_3=$(k3),x1=$x1,x2=$x2,t1=$t1,t2=$t2 
    --level $(level) --width $(width) --verbose 0`
    run(pipeline(cmd, stderr=devnull))
    l_dist[i] = cosmos_get_value("Result_dist_F.res")
end
time_end_cosmos = time()
writedlm(path_results * "/csv/$(exp)_res_average_dist.csv", l_dist, ',')
writedlm(path_results * "/csv/$(exp)_k3.csv", collect(l_k3), ',')

plot(title = "Average computed distance d, $exp", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
plot!(l_k3, l_dist, color = "blue", linestyle = :dash, linewidth=1.0, xlabel = "k3", ylabel = "AVG(Last(d))", label = "")
savefig(path_results * "/pics/$(exp)_average_distance.svg")

file_out = open(path_results * "/std.out", "w")
write(file_out, "Time: $(time_end_cosmos - time_begin_cosmos))\n")
close(file_out)

rm("Result.res")
rm("Result_dist_F.res")

