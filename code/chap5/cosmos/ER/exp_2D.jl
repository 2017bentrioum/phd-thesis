
using Plots
default(show=false, reuse=true)
import DelimitedFiles: writedlm
using Distributed
using Dates
path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
include(path_absolute * "/code/chap5/parser_res.jl")

@assert length(ARGS) > 0 "No arguments entered. Please specify at least one argument that specifies the experiment: R4, R5, R6; e.g. julia exp_1d.jl R3."
@assert length(ARGS) <= 2 "Too much arguments"
exp = ARGS[1]
@assert exp in ["R4", "R5", "R6"] "Wrong name of experiment"
nbr_exp = parse(Int, exp[2]) - 3
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path_cosmos_files = path_absolute * "/code/chap5/cosmos/ER"
if length(ARGS) == 2
    path_results = ARGS[2]
else
    path_results = path_cosmos_files * "/results_$(exp)_$(date_run)"
end
if !isdir(path_results) mkdir(path_results) end
if !isdir(path_results * "/pics") mkdir(path_results * "/pics") end
if !isdir(path_results * "/csv") mkdir(path_results * "/csv") end
#
# Values x1, x2  t1, t2
dict_exp = Dict(
                "R4" => ("dist_F.lha", "x1=5,x2=15,t1=8.0,t2=10.0",  
                         0:0.005:0.05, 0:10:100, "Result_dist_F.res"),
                "R5" => ("dist_G.lha", "x1=50,x2=100,t1=0.0,t2=0.8", 
                         0.0:0.2:2.0, 0:10:100, "Result_dist_G.res"),
                "R6" => ("dist_G_F.lha", "x1=50,x2=100,x3=20,x4=100,t1=0.0,t2=0.8,t3=0.8,t4=0.9", 
                         0.0:0.2:2.0, 0:10:100, "Result_dist_G_F.res")
               )
nb_jobs = haskey(ENV, "SLURM_NTASKS") ? ENV["SLURM_NTASKS"] : nworkers()
width = 0.1
level = 0.99
val_exp = dict_exp[exp]
l_k1 = val_exp[3]
l_k2 = val_exp[4]
nb_k1 = length(l_k1)
nb_k2 = length(l_k2)
mat_dist = zeros(nb_k1,nb_k2)
mat_full_k1 = zeros(nb_k1,nb_k2)
mat_full_k2 = zeros(nb_k1,nb_k2)
time_begin_cosmos = time()
for i in 1:nb_k1
    for j = 1:nb_k2
        k1 = l_k1[i]
        k2 = l_k2[j]
        arg_ctes = val_exp[2]*",k_1=$(k1),k_2=$(k2),k3=1.0"
        cmd = `Cosmos $(path_cosmos_files * "/model.gspn") $(path_cosmos_files * "/" * val_exp[1]) 
        --njob $(nb_jobs)
        --const $(arg_ctes) --level $(level) --width $(width) --verbose 0`
        run(pipeline(cmd, stderr=devnull))
        mat_full_k1[i,j] = k1
        mat_full_k2[i,j] = k2
        mat_dist[i,j] = cosmos_get_value(val_exp[5])
    end
end
time_end_cosmos = time()
writedlm(path_results * "/csv/$(exp)_res_average_dist.csv", mat_dist, ',')
writedlm(path_results * "/csv/$(exp)_k1.csv", mat_full_k1, ',')
writedlm(path_results * "/csv/$(exp)_k2.csv", mat_full_k2, ',')

plot(title = "Average computed distance d, $exp", background_color_legend = :transparent, dpi = 480, legend = :outertopright)
#plot!(l_k1, l_k2, mat_dist, st = :surface, c = :coolwarm, camera = (30, 45))
@show length(vec(mat_full_k1)), length(vec(mat_full_k2)), length(vec(mat_dist))
plot!(vec(mat_full_k1), vec(mat_full_k2), vec(mat_dist), st = :surface, c = :coolwarm, camera = (30, 45), xlabel = "k1", ylabel = "k2")
#plot!(ylims = (0,))
savefig(path_results * "/pics/$(exp)_average_distance.svg")

file_out = open(path_results * "/std.out", "w")
write(file_out, "Time: $(time_end_cosmos - time_begin_cosmos))\n")
close(file_out)

rm("Result.res")
rm(val_exp[5])

