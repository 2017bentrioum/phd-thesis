
using DelimitedFiles

path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_csv = (path_absolute != ".") ? path_absolute * "/code/chap5/julia/estim_MC/sir_ki_kr" : "."
if !isdir(path_csv) mkdir(path_csv) end
include(path_absolute * "/code/chap5/parser_res.jl")

l_ki = 5E-4:2.5E-4:3E-3
l_kr = 5E-3:0.02:0.2
nb_ki = length(l_ki)
nb_kr = length(l_kr)
mat_ki = zeros(nb_ki, nb_kr)
mat_kr = zeros(nb_ki, nb_kr)
mat_prob = zeros(nb_ki, nb_kr)

for i = eachindex(l_ki)
    for j = eachindex(l_kr)
        ki, kr = l_ki[i], l_kr[j]
        mat_ki[i,j], mat_kr[i,j] = ki, kr
        command = `prism sir.sm G_F.csl --const Npop=100,ki=$(ki),kr=$(kr) -exportresults res_prism.out`
        run(pipeline(command, stdout=devnull))
        file_res = open("res_prism.out", "r")
        line = readline(file_res)
        line = readline(file_res) 
        val_prob = parse(Float64, line)
        close(file_res)
        mat_prob[i,j] = val_prob
    end
end

rm("res_prism.out")
writedlm(path_csv * "/grid_X_mc.csv", mat_ki, ',')
writedlm(path_csv * "/grid_Y_mc.csv", mat_kr, ',')
writedlm(path_csv * "/satisfaction_func_mc.csv", mat_prob, ',')

