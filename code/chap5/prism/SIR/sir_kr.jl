
using DelimitedFiles

path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_csv = (path_absolute != ".") ? path_absolute * "/code/chap5/julia/estim_MC/sir_kr" : "."
if !isdir(path_csv) mkdir(path_csv) end
include(path_absolute * "/code/chap5/parser_res.jl")

l_kr = 0.005:0.005:0.2
l_prob = zeros(0)
time_begin = time()
for kr in l_kr
    command = `prism sir.sm G_F.csl --const Npop=100,ki=0.0012,kr=$(kr) -exportresults res_prism.out`
    run(pipeline(command, stdout=devnull))
    file_res = open("res_prism.out", "r")
    line = readline(file_res)
    line = readline(file_res) 
    val_prob = parse(Float64, line)
    close(file_res)
    push!(l_prob, val_prob)
end
time_end = time()
rm("res_prism.out")
writedlm(path_csv * "/kr_mc.csv", collect(l_kr), ',')
writedlm(path_csv * "/satisfaction_func_mc.csv", l_prob, ',')

file_out = open(path_csv * "/stdout_mc.out", "w")
write(file_out, "Time : $(time_end - time_begin)\n")
close(file_out)

