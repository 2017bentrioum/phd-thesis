
using DelimitedFiles

@assert length(ARGS) == 1 "Incorrect number of arguments"
exp = ARGS[1]
@assert exp in ["R4", "R5", "R6"] "Incorrect name of experience."
path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_csv = (path_absolute != ".") ? path_absolute * "/code/chap5/julia/estim_MC/$(exp)" : "."
path_prism_files = path_absolute * "/code/chap5/prism/ER"
model_file = path_prism_files * "/er.sm"
if !isdir(path_csv) mkdir(path_csv) end
include(path_absolute * "/code/chap5/parser_res.jl")
dict_const_exp = Dict("R4" => "x1=5.0,x2=15.0,t1=8.0,t2=10.0",
                      "R5" => "x1=50.0,x2=100.0,t1=0.0,t2=0.8",
                      "R6" => "x1=50.0,x2=100.0,t1=0.0,t2=0.0,x3=30.0,x4=100.0,t3=0.8,t4=0.9")
dict_prop_file = Dict("R4" => "phi_F.csl", "R5" => "phi_G.csl", "R6" => "phi_G_F.csl")
str_const = dict_const_exp[exp]
prop_file = path_prism_files * "/" * dict_prop_file[exp]

dict_k1 = Dict("R4" => 0.0:0.005:0.05, "R5" => 0.0:0.2:2.0, "R6" => 0.0:0.15:1.5)
l_k1 = dict_k1[exp]
l_k2 = 0:5.0:100.0
nb_k1 = length(l_k1)
nb_k2 = length(l_k2)
mat_k1 = zeros(nb_k1, nb_k2)
mat_k2 = zeros(nb_k1, nb_k2)
mat_prob = zeros(nb_k1, nb_k2)
time_begin = time()
for i = eachindex(l_k1)
    for j = eachindex(l_k2)
        k1, k2 = l_k1[i], l_k2[j]
        mat_k1[i,j], mat_k2[i,j] = k1, k2
        command = `prism $(model_file) $(prop_file) -sim -simmethod apmc -simapprox 0.01 -simconf 0.99 --const N=100,k1=$(k1),k2=$(k2),k3=1.0,$(str_const) -exportresults res_prism_$(exp).out`
        run(pipeline(command, stdout=devnull))
        file_res = open("res_prism_$(exp).out", "r")
        line = readline(file_res)
        line = readline(file_res) 
        val_prob = parse(Float64, line)
        close(file_res)
        mat_prob[i,j] = val_prob
    end
    println("k1 = $(l_k1[i]), k2 in $l_k2 done")
end
time_end = time()
rm("res_prism_$(exp).out")

writedlm(path_csv * "/grid_X_smc.csv", mat_k1, ',')
writedlm(path_csv * "/grid_Y_smc.csv", mat_k2, ',')
writedlm(path_csv * "/satisfaction_func_smc.csv", mat_prob, ',')

file_out = open(path_csv * "/stdout_smc.out", "w")
write(file_out, "Time : $(time_end - time_begin)\n")
close(file_out)

