
using DelimitedFiles

@assert length(ARGS) == 1 "Incorrect number of arguments"
exp = ARGS[1]
@assert exp in ["R1", "R2", "R3"] "Incorrect name of experience."
path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_csv = (path_absolute != ".") ? path_absolute * "/code/chap5/julia/estim_MC/$(exp)" : "."
path_prism_files = path_absolute * "/code/chap5/prism/ER"
model_file = path_prism_files * "/er.sm"
prop_file = path_prism_files * "/phi_F.csl"
if !isdir(path_csv) mkdir(path_csv) end
# (x1, x2, t1, t2)
dict_constants_exp = Dict("R1" => (50.0, 75.0, 0.025, 0.05),
                          "R2" => (50.0, 75.0, 0.05, 0.075),
                          "R3" => (25.0, 50.0, 0.05, 0.075))
ctes = dict_constants_exp[exp]
l_k3 = (exp == "R3") ? (0.0:2.0:40.0) : (0.0:5.0:100.0)
l_prob = zeros(0)
time_begin = time()
for k3 in l_k3
    command = `prism $model_file $prop_file --const N=100,k1=1.0,k2=1.0,k3=$(k3),x1=$(ctes[1]),x2=$(ctes[2]),t1=$(ctes[3]),t2=$(ctes[4]) -exportresults res_prism_$(exp).out`
    run(pipeline(command, stdout=devnull))
    file_res = open("res_prism_$(exp).out", "r")
    line = readline(file_res)
    line = readline(file_res) 
    val_prob = parse(Float64, line)
    close(file_res)
    push!(l_prob, val_prob)
    println("k3 = $k3 done")
end
time_end = time()
rm("res_prism_$(exp).out")
writedlm(path_csv * "/k3_mc.csv", collect(l_k3), ',')
writedlm(path_csv * "/satisfaction_func_mc.csv", l_prob, ',')

file_out = open(path_csv * "/stdout_mc.out", "w")
write(file_out, "Time : $(time_end - time_begin)\n")
close(file_out)

