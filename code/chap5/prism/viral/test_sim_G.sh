#!/bin/sh

prism viral.sm --const cn=0.85,ca=1.3 -simpath time=200.0 path.out -simpathlen 1000000
julia min_plot.jl
eog G.svg

