
using DelimitedFiles

path_absolute = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_csv = (path_absolute != ".") ? path_absolute * "/code/chap5/julia/estim_MC/viral_cn_ca" : "."
if !isdir(path_csv) mkdir(path_csv) end
include(path_absolute * "/code/chap5/parser_res.jl")

l_cn = 0.6:0.1:1.1
l_ca = 0.5:0.2:2.0
l_cn, l_ca = [0.85], [1.3]
nb_cn = length(l_cn)
nb_ca = length(l_ca)
mat_cn = zeros(nb_cn, nb_ca)
mat_ca = zeros(nb_cn, nb_ca)
mat_prob = zeros(nb_cn, nb_ca)

for i = eachindex(l_cn)
    for j = eachindex(l_ca)
        cn, ca = l_cn[i], l_ca[j]
        mat_cn[i,j], mat_ca[i,j] = cn, ca
        command = `prism viral.sm G_F.csl -const cn=$(cn),ca=$(ca) -sim -simpathlen 10000000 
        -simmethod apmc -simapprox 0.01 -simconf 0.95 -exportresults res_prism.out`
        #run(pipeline(command, stdout=devnull))
        run(command)
        file_res = open("res_prism.out", "r")
        line = readline(file_res)
        line = readline(file_res) 
        val_prob = parse(Float64, line)
        close(file_res)
        mat_prob[i,j] = val_prob
    end
end

rm("res_prism.out")
writedlm(path_csv * "/grid_X_smc.csv", mat_cn, ',')
writedlm(path_csv * "/grid_Y_smc.csv", mat_ca, ',')
writedlm(path_csv * "/satisfaction_func_smc.csv", mat_prob, ',')

