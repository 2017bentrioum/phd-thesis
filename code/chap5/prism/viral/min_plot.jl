
using Plots
using DelimitedFiles

tab = readdlm("path.out", ' ')
times = tab[3:end, 3] 
vec_G = tab[3:end, 6]
times = convert(Vector{Float64}, times)
vec_G = convert(Vector{Float64}, vec_G)

plot(times, vec_G, linetype = :steppost, xlims = (0, 200))
hline!([10.0], color = :red)
hline!([100.0], color = :green)
vline!([50.0], color = :black)
savefig("G.svg")

