
using MarkovProcesses
using DelimitedFiles
using Plots

path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_csv = path * "/code/chap1/"
path_pics = path * "/pictures/chap1/"

load_model("SIR")
load_model("SIR_tauleap")

N_pop = 100
set_param!(SIR, [0.12/N_pop, 0.05])
set_param!(SIR_tauleap, [0.12/N_pop, 0.05, 0.5])
set_time_bound!(SIR, 100.0)
set_time_bound!(SIR_tauleap, 100.0)

mat_sim_ode = readdlm(path_csv * "sim_ode_sir_100.csv", ',')
x_ode = mat_sim_ode[:,1]
y_ode = mat_sim_ode[:,2]
nb_sim = 5

# Plot SSA + Tau 0.5 + ODE, 100 pop
plot(title = "SIR", xlabel = "Time", ylabel = "Number of people", dpi = 480)
for i = 1:nb_sim
    label_exact = (i == 1) ? "Exact" : ""
    label_tau = (i == 1) ? "Tau leap" : ""
    σ = simulate(SIR)
    σ_tau = simulate(SIR_tauleap)
    plot!(times(σ), σ[:I], color = "red", linetype = :steppost, label = label_exact)
    plot!(times(σ_tau), σ_tau[:I], color = "green", linetype = :steppost, label = label_tau)
end
plot!(x_ode, y_ode, color = "blue", label = "RRE (ODE)")
savefig(path_pics * "plot_ode_ssa_tau_100_0_5.svg")

set_param!(SIR_tauleap, :tau, 5.0)
# Plot SSA + Tau 5.0 + ODE, 100 pop
plot(title = "SIR", xlabel = "Time", ylabel = "Number of people", dpi = 480)
for i = 1:nb_sim
    label_exact = (i == 1) ? "Exact" : ""
    label_tau = (i == 1) ? "Tau leap" : ""
    σ = simulate(SIR)
    σ_tau = simulate(SIR_tauleap)
    plot!(times(σ), σ[:I], color = "red", linetype = :steppost, label = label_exact)
    plot!(times(σ_tau), σ_tau[:I], color = "green", linetype = :steppost, label = label_tau)
end
plot!(x_ode, y_ode, color = "blue", label = "RRE (ODE)")
savefig(path_pics * "plot_ode_ssa_tau_100_5_0.svg")

mat_sim_ode = readdlm(path_csv * "sim_ode_sir_10000.csv", ',')
x_ode = mat_sim_ode[:,1]
y_ode = mat_sim_ode[:,2]
N_pop = 10000
set_param!(SIR, [0.12/N_pop, 0.05])
set_param!(SIR_tauleap, [0.12/N_pop, 0.05, 5.0])
set_x0!(SIR, [9500,500,0])
set_x0!(SIR_tauleap, [9500,500,0])
# Plot SSA + Tau 10.0 + ODE, 10000 pop
plot(title = "SIR", xlabel = "Time", ylabel = "Number of people", dpi = 480)
for i = 1:nb_sim
    label_exact = (i == 1) ? "Exact" : ""
    label_tau = (i == 1) ? "Tau leap" : ""
    σ = simulate(SIR)
    σ_tau = simulate(SIR_tauleap)
    plot!(times(σ), σ[:I], color = "red", linetype = :steppost, label = label_exact)
    plot!(times(σ_tau), σ_tau[:I], color = "green", linetype = :steppost, label = label_tau)
end
plot!(x_ode, y_ode, color = "blue", label = "RRE (ODE)")
savefig(path_pics * "plot_ode_ssa_tau_10000_5_0.svg")

