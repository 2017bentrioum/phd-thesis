
using DelimitedFiles
using StatsBase
using Statistics

samples_abc_post = readdlm("mat_p_end.csv", ',')
weights = vec(readdlm("weights_end.csv", ','))
samples_weights = pweights(weights)

for i = 1:size(samples_abc_post, 1)
    println("## Param $i")
    p_mean = mean(samples_abc_post[i,:], samples_weights)
    p_var = var(samples_abc_post[i,:], samples_weights)
    p_std = std(samples_abc_post[i,:], samples_weights)
    p_skewness = skewness(samples_abc_post[i,:], samples_weights)
    p_kurtosis = kurtosis(samples_abc_post[i,:], samples_weights)
    println("Mean:$(p_mean)\nVar:$(p_var)\nStd:$(p_std)")
    println("Skewness:$(p_skewness)\nKurtosis:$(p_kurtosis)")
end
println("Covariance matrix:")
mat_cov = cov(samples_abc_post, samples_weights, 2)
println(mat_cov)

