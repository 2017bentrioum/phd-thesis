
using DelimitedFiles
using Plots

@assert length(ARGS) == 1
exp = ARGS[1]
bins_option = 25

filename_abc_post = "mat_p_end.csv"
filename_abc_weights = "weights_end.csv"
abc_post = readdlm(filename_abc_post, ',')
abc_weights = vec(readdlm(filename_abc_weights, ','))

# plotattr is helpful

histogram2d(abc_post[1,:], abc_post[2,:], weights = abc_weights, nbins = 25,
            #xguidefontsize = 18, yguidefontsize = 18,
            #legendfontsize = 18,
            xtickfontsize = 16, ytickfontsize = 16,
            xtickfonthalign = :right,
            tickfonthalign = :right,
            tickfontvalign = :top,
            ytickfonthalign = :top,
            right_margin = 15Plots.mm)
savefig("$(exp)_hist_abc.svg")

