
using DelimitedFiles
using BoundedKDE
using Plots
import Statistics: mean
using Distributions
gr()

function poisson_pr_func(λ::Float64, x1::Float64, x2::Float64, t1::Float64, t2::Float64)
    val = 0.0
    N_t1 = Poisson(λ*t1)
    N_t2 = Poisson(λ*t2)
    N_t2mt1 = Poisson(λ*(t2-t1))
    for i = 0:x2
        val += (1-cdf(N_t2mt1, x1-i-1))*pdf(N_t1, i)
    end
    return val
end
poisson_pr_func(λ::Float64) = poisson_pr_func(λ, 5.0, 7.0, 0.75, 1.0)

@assert length(ARGS) == 4 "Error in number of arguments: [exp] [kernel: gaussian|chen99] [bandwidth : number] [output picture name]"
exp_name = ARGS[1]
kernel_kde = ARGS[2]
coefft_bandwidth = parse(Float64, ARGS[3])
picture_name = ARGS[4]

exps_p_star = Dict("R1" => [80.0, 50.0], "R2" => [25.0], "R3" => [10.0], "sir_kr" => [0.08], "poisson" => [7.0, 7.5])
exps_prob_p_star = Dict("R1" => [1.0, 1.0], "R2" => [1.0], "R3" => [0.9997], "sir_kr" => [0.1911610888097025], "poisson" => poisson_pr_func.(exps_p_star["poisson"]))
legend_name = (exp_name == "sir_kr") ? "SIR" : "T$(exp_name)"

if exp_name != "poisson"
    xaxis = (exp_name == "sir_kr") ? (3E-3:2E-3:0.2) : (0.0:1.0:100.0)
    param_file = (exp_name == "sir_kr") ? "kr_mc.csv" : "k3_mc.csv"
    xaxis_mc = readdlm(ENV["BENTRIOU_THESIS"] * "/code/chap5/julia/estim_MC/$(exp_name)/$(param_file)", ',')
    values_mc = readdlm(ENV["BENTRIOU_THESIS"] * "/code/chap5/julia/estim_MC/$(exp_name)/satisfaction_func_mc.csv", ',')
else
    legend_name = "Poisson"
    xaxis = 0:0.3:30.0
end
vec_abc = vec(readdlm("mat_p_end.csv",','))
abc_weights = vec(readdlm("weights_end.csv",','))

lbound, ubound = 0.0, 100.0
if exp_name == "sir_kr"
    lbound, ubound = 5E-3, 0.2
elseif exp_name == "poisson"
    lbound, ubound = 0.0, 30.0
end
estim_abc_post = UnivariateKDE(vec_abc; kernel = kernel_kde, weights = abc_weights, 
                               lower_bound = lbound, upper_bound = ubound)
bandwidth = coefft_bandwidth * asymptotic_bandwidth(estim_abc_post)
estim_abc_post = change_bandwidth(estim_abc_post, bandwidth)
pdf_estim_abc_post_pkg(x) = pdf(estim_abc_post, x)
constant = mean(exps_prob_p_star[exp_name] ./ pdf_estim_abc_post_pkg.(exps_p_star[exp_name]))
prob_func(x) = pdf_estim_abc_post_pkg(x) * constant

p = plot(title = "Satisfaction probability function for $legend_name", 
         legend = nothing, background_color_legend = :transparent, dpi = 480)
#histogram!(vec_abc, weights = weights, yaxis = :right)
#plotattr()
if exp_name != "poisson" 
    plot!(p, xaxis_mc, values_mc, marker = :cross, markersize = 2.0, label = "MC")
else
    plot!(p, xaxis, poisson_pr_func, marker = :cross, markersize = 2.0, label = "MC")
end
plot!(p, xaxis, prob_func(xaxis), label = "Estimated spf")
subplot = twinx(p)
plot!(subplot, vec_abc, color = "green", weights = abc_weights, line = :histogram, 
      alpha = 0.3, legend = nothing, label = "")
savefig(picture_name)

