
using DelimitedFiles
using BoundedKDE
using Plots

@assert length(ARGS) == 3 || length(ARGS) == 4 "Error in number of arguments: [exp] [kernel: gaussian|chen99] [bandwidth : vector] [optional: output picture name]"
filename_abc_post = "mat_p_end.csv"
filename_abc_weights = "weights_end.csv"
exp = ARGS[1]
kernel_name = ARGS[2]
coefft_bandwidth = ARGS[3]
picture_name = (length(ARGS) == 4) ? ARGS[4] : "$(exp)_spf_abc.svg"

exps_p_star = Dict("R4" => [[0.01, 95.0], [0.005, 40.0], [0.015, 80.0]], "R5" => [[0.25, 40.0], [0.4, 95.0]], "R6" => [[0.45, 45.0], [0.9, 75.0]], "sir_ki_kr" => [[0.0012, 0.08], [0.00225, 0.065]], "viral_cn_ca" => [[0.7, 0.65], [1.0, 1.85], [1.0, 0.65]])
exps_prob_p_star = Dict("R4" => [0.97127, 0.974971, 0.75398], "R5" => [1.0, 1.0], "R6" => [0.4954, 0.25398], "sir_ki_kr" => [0.19116, 0.330139], "viral_cn_ca" => [0.39423, 0.2240, 0.21848])
exps_density_lbound = Dict("R4" => [0.0, 0.0], "R5" => [0.0, 0.0], "R6" => [0.0, 0.0], "sir_ki_kr" => [5E-4, 5E-3], "viral_cn_ca" => [0.6, 0.5])
exps_density_ubound = Dict("R4" => [0.05, 100.0], "R5" => [2.0, 100.0], "R6" => [2.0, 100.0], "sir_ki_kr" => [3E-3, 0.2], "viral_cn_ca" => [1.1, 2.0])
exps_legend_name = Dict("R4" => "T$(exp)", "R5" => "T$(exp)", "R6" => "T$(exp)", "sir_ki_kr" => "SIR", "viral_cn_ca" => "viral infection")

density_lbound = exps_density_lbound[exp]
density_ubound = exps_density_ubound[exp]
legend_name = exps_legend_name[exp]

abc_post = readdlm(filename_abc_post, ',')
abc_weights = vec(readdlm(filename_abc_weights, ','))
coefft_bandwidth = eval(Meta.parse(coefft_bandwidth))
estim_abc_post = MultivariateKDE(abc_post; kernel = kernel_name, weights = abc_weights,
                                 lower_bound = density_lbound, upper_bound = density_ubound)
bandwidth = coefft_bandwidth .* asymptotic_bandwidth(estim_abc_post)
estim_abc_post = change_bandwidth(estim_abc_post, bandwidth)
pdf_estim_abc_post(vec_x) = pdf(estim_abc_post, vec_x)
constant = mean(exps_prob_p_star[exp] ./ pdf_estim_abc_post.(exps_p_star[exp]))
prob_func(x,y) = pdf_estim_abc_post([x,y]) * constant
@show estim_abc_post.bandwidth, estim_abc_post.kernel, constant
@show prob_func(exps_p_star[exp][1]...), exps_prob_p_star[exp][1]
@show prob_func(exps_p_star[exp][2]...), exps_prob_p_star[exp][2]

exps_xaxis = Dict("R4" => 0:0.0005:0.05, "R5" => 0:0.015:1.5, "R6" => 0:0.015:1.5, "sir_ki_kr" => 5E-4:(4.5E-5):3E-3, "viral_cn_ca" => 0.6:0.05:1.1)
exps_yaxis = Dict("R4" => 0.0:1.0:100.0, "R5" => 0.0:1.0:100.0, "R6" => 0.0:1.0:100.0, "sir_ki_kr" => 5E-3:2E-3:0.2, "viral_cn_ca" => 0.5:0.05:2.0)
exps_xlabel = Dict("R4" => "k1", "R5" => "k1", "R6" => "k1", "sir_ki_kr" => "ki", "viral_cn_ca" => "cn")
exps_ylabel = Dict("R4" => "k2", "R5" => "k2", "R6" => "k2", "sir_kr_kr" => "kr", "viral_cn_ca" => "ca")
xlabel, ylabel = exps_xlabel[exp], exps_ylabel[exp]
xaxis = exps_xaxis[exp]
yaxis = exps_yaxis[exp]
p = plot(title = "Satisfaction function of $(legend_name) with automaton-ABC", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
plot!(p, xaxis, yaxis, prob_func, st = :surface, c = :coolwarm, camera = (30, 45),
      tickfontsize = 13, ytickfontvalign = :top,
      ytickfonthalign = :right)
if exp_name in ["R4", "R5", "R6"]
    plot!(yticks = 20:20:100)
end
savefig(picture_name)

histogram2d(abc_post[1,:], abc_post[2,:], weights = abc_weights, nbins = 25)
savefig("$(exp)_hist_abc.svg")

