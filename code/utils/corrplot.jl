
using DelimitedFiles
using Plots
using StatsPlots

@assert length(ARGS) == 2 "Wrong number of arguments: \$corrplot [labels] [output picture name]"
labels = eval(Meta.parse(ARGS[1]))
labels = [String(label) for label in labels]

samples_abc_post = readdlm("mat_p_end.csv", ',')
weights = vec(readdlm("weights_end.csv", ','))
#corrplot(samples_abc_post', label = labels, weights = weights)
corrplot(samples_abc_post', label = labels, fillcolor = cgrad())
savefig(ARGS[2])

