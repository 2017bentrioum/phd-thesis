
using DelimitedFiles
using Plots
gr()

@assert length(ARGS) == 2
exp_name = ARGS[1]
picture_name = ARGS[2]
mat_xaxis = readdlm("csv/$(exp_name)_k1.csv", ',')
mat_yaxis = readdlm("csv/$(exp_name)_k2.csv", ',')
mat_values = readdlm("csv/$(exp_name)_res_average_dist.csv", ',')

plot(title = "Average computed distance d from T$exp_name", background_color_legend = :transparent, dpi = 480, legend = :outertopright)
plot!(vec(mat_xaxis), vec(mat_yaxis), vec(mat_values), st = :surface, c = :coolwarm, camera = (30, 60), xlabel = "k1", ylabel = "k2")
savefig(picture_name)

