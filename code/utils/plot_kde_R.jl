
using DelimitedFiles
using Plots
import GR
gr()

@assert length(ARGS) == 2
exp_name = ARGS[1]
picture_name = ARGS[2]
endfile_csv = "smc"
legend_name = "T$(exp_name)"
xlabel, ylabel = "k1", "k2"
if exp_name == "sir_ki_kr"
    endfile_csv = "mc"
    legend_name = "SIR"
    xlabel, ylabel = "ki", "kr"
elseif exp_name == "viral_cn_ca"
    legend_name = "viral infection"
    xlabel, ylabel = "cn", "ca"
end
if exp_name == "R4_zoom"
    exp_name = "R4"
    endfile_csv = "smc_zoom"
end
mat_grid, h_grid = readdlm(ENV["BENTRIOU_THESIS"] * "/code/chap5/julia/density_estim_R/$(exp_name)/grid.csv", ',', header = true)
mat_values, h_values = readdlm(ENV["BENTRIOU_THESIS"] * "/code/chap5/julia/density_estim_R/$(exp_name)/values.csv", ',', header = true)

plot(title = "KDE of $(legend_name) with $(uppercase(endfile_csv))", 
     background_color_legend = :transparent, dpi = 480, legend = :outertopright)
plot!(mat_grid[:,1], mat_grid[:,2], mat_values[:,1], 
      st = :surface, c = :coolwarm, camera = (30, 45), 
      xlabel = h_grid[1], ylabel = h_grid[2], display_option = GR.OPTION_Z_SHADED_MESH,
      tickfontsize = 13, ytickfontvalign = :top,
      ytickfonthalign = :right)
savefig(picture_name)


