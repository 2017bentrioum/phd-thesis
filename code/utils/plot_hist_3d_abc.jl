
using DelimitedFiles
using Plots

@assert length(ARGS) == 3
exp = ARGS[1]
bins_option = ARGS[2]
picture_name = ARGS[3]

samples_abc_post = readdlm("mat_p_end.csv", ',')
weights = vec(readdlm("weights_end.csv", ','))

exp_labels = Dict("doping_3way_3d" => ["rA", "rB", "rC"])
labels = exp_labels[exp]
bins_option = bins_option[1] == ":" ? convert(Symbol, bins_option) : parse(Int, bins_option)
hist1 = histogram2d(samples_abc_post[[1,2],:]', weights = weights, xlabel = labels[1], ylabel = labels[2], normalize = false, bins = bins_option)
hist2 = histogram2d(samples_abc_post[[1,3],:]', weights = weights, xlabel = labels[1], ylabel = labels[3], normalize = false, bins = bins_option)
hist3 = histogram2d(samples_abc_post[[2,3],:]', weights = weights, xlabel = labels[2], ylabel = labels[3], normalize = false, bins = bins_option)
p = plot(hist1, hist2, hist3, layout = 3)
savefig(p, picture_name)

