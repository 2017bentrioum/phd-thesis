
using DelimitedFiles
using Plots
using StatsPlots

@assert length(ARGS) == 3 "Wrong number of arguments: \$marginalkde [xlabel] [ylabel] [output picture name]"

samples_abc_post = readdlm("mat_p_end.csv", ',')
weights = vec(readdlm("weights_end.csv", ','))
x = samples_abc_post[1,:]
y = samples_abc_post[2,:]

marginalkde(x, y, xlabel = ARGS[1], ylabel = ARGS[2], weights = weights)
savefig(ARGS[3])

