
using DelimitedFiles
using BoundedKDE
using Plots
using StatsPlots
import Statistics: mean
using Distributions
gr()

@assert length(ARGS) == 1 "Wrong number of arguments: [output picture name]"

vec_abc = vec(readdlm("mat_p_end.csv",','))
abc_weights = vec(readdlm("weights_end.csv",','))

p = plot(title = "ABC posterior", background_color_legend = :transparent, dpi = 480)

plot!(p, vec_abc, color = "green", weights = abc_weights, line = :histogram, 
      alpha = 0.3, legend = nothing, label = "")
xlim = maximum(vec_abc)*1.01
xaxis = 0.0:(xlim/1000):xlim
subplot = twinx(p)
#=
estim_abc_post = UnivariateKDE(vec_abc; kernel = "gaussian", weights = abc_weights)
lscv_bandwidth = minimize_lscv(estim_abc_post, 0.1, 10.0; verbose = true, rel_tol = 1e-8)
@show lscv_bandwidth
estim_abc_post = change_bandwidth(estim_abc_post, lscv_bandwidth)
estim_pdf(x) = pdf(estim_abc_post, x)
plot!(subplot, xaxis, estim_pdf.(xaxis), color = "red", label = "KDE")
=#
density!(subplot, vec_abc, weights = abc_weights, color = "red", label = "KDE")
savefig(ARGS[1])

