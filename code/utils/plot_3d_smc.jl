
using DelimitedFiles
using Plots
import GR
gr()

@assert length(ARGS) == 2
exp_name = ARGS[1]
picture_name = ARGS[2]
endfile_csv = "smc"
legend_name = "T$(exp_name)"
xlabel, ylabel = "k1", "k2"
if exp_name == "sir_ki_kr"
    endfile_csv = "mc"
    legend_name = "SIR"
    xlabel, ylabel = "ki", "kr"
elseif exp_name == "viral_cn_ca"
    legend_name = "viral infection"
    xlabel, ylabel = "cn", "ca"
end
if exp_name == "R4_zoom"
    exp_name = "R4"
    endfile_csv = "smc_zoom"
end
mat_xaxis = readdlm(ENV["BENTRIOU_THESIS"] * "/code/chap5/julia/estim_MC/$(exp_name)/grid_X_$(endfile_csv).csv", ',')
mat_yaxis = readdlm(ENV["BENTRIOU_THESIS"] * "/code/chap5/julia/estim_MC/$(exp_name)/grid_Y_$(endfile_csv).csv", ',')
mat_values = readdlm(ENV["BENTRIOU_THESIS"] * "/code/chap5/julia/estim_MC/$(exp_name)/satisfaction_func_$(endfile_csv).csv", ',')

plot(title = "Satisfaction function of $(legend_name) with $(uppercase(endfile_csv))", 
     background_color_legend = :transparent, dpi = 480, legend = :outertopright)
plot!(vec(mat_xaxis), vec(mat_yaxis), vec(mat_values), 
      st = :surface, c = :coolwarm, camera = (30, 45), 
      xlabel = xlabel, ylabel = ylabel, display_option = GR.OPTION_Z_SHADED_MESH,
      tickfontsize = 13, ytickfontvalign = :top,
      ytickfonthalign = :right)
if exp_name in ["R4", "R5", "R6"]
    plot!(yticks = 20:20:100)
end
if exp_name in ["R4_zoom"]
    plot!(yticks = 5:5:20)
end
#println(vec(mat_xaxis))
#println(vec(mat_yaxis))
#surface!(vec(mat_xaxis), vec(mat_yaxis), vec(mat_values), c = :coolwarm, camera = (30, 45), xlabel = xlabel, ylabel = ylabel, display_option=GR.OPTION_LINES)
savefig(picture_name)

