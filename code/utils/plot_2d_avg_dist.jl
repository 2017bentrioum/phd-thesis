
using DelimitedFiles
using Plots
gr()

@assert length(ARGS) == 2
exp_name = ARGS[1]
picture_name = ARGS[2]
xaxis = readdlm("csv/$(exp_name)_k3.csv", ',')
values = readdlm("csv/$(exp_name)_res_average_dist.csv", ',')

plot(title = "Average computed distance d from T$exp_name", background_color_legend = :transparent, dpi = 480, legend = nothing)
plot!(vec(xaxis), vec(values), color = "blue", linestyle = :dash, linewidth=1.0,
                               marker = :cross, markersize = 3.0,
                               xlabel = "k3", ylabel = "AVG(Last(d))", label = "")
savefig(picture_name)

