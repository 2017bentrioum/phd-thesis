
if VERSION >= v"0.7.0"
    @everywhere using Distributed
    @everywhere using Random
    using DelimitedFiles
end
import Statistics: std
@everywhere using pygmalion
include(ENV["PYGMALION_DIR"] * "etc/plot.jl")
include(ENV["PYGMALION_DIR"] * "algorithms/estimation/abc_pop_mc.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/estimation/helpers/step_func.jl")

function redirect_to_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end

@everywhere str_m = "sir_ctmc"
str_d = "abc_sir_ctmc_dist_int"
str_dir =  create_results_directory(; title = str_d)
@everywhere load_model(str_m)
on = nothing
mn = nothing
str_oml = "X_S,X_I,X_R,R,time"
ll_om = split(str_oml, ",")
# Set-up
@everywhere N_x0 = 100.0
@everywhere s0 = 0.95
@everywhere S_0 = floor(N_x0 * s0)
@everywhere x0 = State(S_0, N_x0 - S_0, 0.0, 0.0, 0.0)
p_true = Parameters(0.12 / N_x0, 0.05)
const nb_pts_obs = 50
const t_end = 150.0
tml = 1:100
g_all = create_observation_function([ObserverModel(str_oml, tml)])
g_obs = create_observation_function([ObserverModel("X_I,time", tml)])
u = Control(t_end)
nb_traj = 5
l_so_exp = SystemObservationList(undef, nb_traj)
for i = 1:nb_traj
    l_so_exp[i] = load_from_obs(ENV["PYGMALION_DIR"] * "database/observations/sir_ctmc/0-12_0-05_$i.obs")
end

# Config ABC
cfg_ll_p = ["ki", "kr"]
#cfg_ll_p = ["kr"]
cfg_srl_p = [
             SamplingRule(["ki"], "uniform", (0.005 / N_x0, 0.3 / N_x0)), 
             SamplingRule(["kr"], "uniform", (0.005, 0.2))
            ]
cfg_bl_p = [
            Bound("ki", 0.0, 0.3 / N_x0), 
            Bound("kr", 0.0, 0.2)
           ]
cfg_nbr_particles = 100
# SS functions
@everywhere function so2vec_I(so::SystemObservation, x0::State)
    idx_om = om_findfirst("X_I", so.oml)
    nb_pts = length(so.otll[idx_om])+1
    vec_so = zeros(nb_pts)
    for i = 1:nb_pts
        vec_so[i] = get_traj(so, idx_om, x0, i)
    end
    return vec_so
end
@everywhere so2vec_I(so::SystemObservation) = so2vec_I(so, x0)
@everywhere function so2vec_time(so::SystemObservation, x0::State)
    idx_time = om_findfirst("time", so.oml)
    nb_pts = length(so.otll[idx_time])+1
    vec_so = zeros(nb_pts)
    for i = 1:nb_pts
        vec_so[i] = get_traj(so, idx_time, x0, i)
    end
    return vec_so
end
@everywhere so2vec_time(so::SystemObservation) = so2vec_time(so, x0)
cfg_eta_function = identity_l_so
# Lp distance over step functions
@everywhere l1_step(so_sim::Union{Nothing, SystemObservation},
        so_exp::Union{Nothing, SystemObservation}) = l1_step(so_sim, so_exp, x0, "X_I")
@everywhere l2_step(so_sim::Union{Nothing, SystemObservation},
        so_exp::Union{Nothing, SystemObservation}) = l2_step(so_sim, so_exp, x0, "X_I")
@everywhere function l2_step(l_so_sim::Union{Nothing, SystemObservationList}, 
                 l_so_exp::Union{Nothing, SystemObservationList})
    d_min = Inf
    for so_sim in l_so_sim
        for so_exp in l_so_exp
            x_obs = so2vec_I(so_exp)
            t_x = so2vec_time(so_exp)
            y_obs = so2vec_I(so_sim)
            t_y = so2vec_time(so_sim)
            d_current = l2_step(x_obs, t_x, y_obs, t_y)
            # d_current = l2_step(so_sim, so_exp)
            if d_current < d_min d_min = d_current end
        end
    end
    return d_min
end
cfg_distance_function = l2_step
cfg_str_statistical_measure = "mean"
# To compute an suitable end epsilon
norm_y_exp = Inf
for so_exp in l_so_exp
    y_exp = to_vec(so_exp, "X_I")
    y_exp = vcat(x0.X_I, y_exp)
    t_y_exp = to_vec(so_exp, "time")
    t_y_exp = vcat(x0.time, t_y_exp)
    y_zero = [0.0, 0.0]
    t_y_zero = [0.0, t_end]
    current_dist = l2_step(y_exp, t_y_exp, y_zero, t_y_zero)
    if current_dist < norm_y_exp 
        global norm_y_exp = current_dist 
    end
end
prct_eps = 0.0
cfg_end_epsilon = prct_eps * norm_y_exp
cfg_l_epsilon = [cfg_end_epsilon]
cfg_alpha = 0.75
cfg_kernel_type = "mv_normal"
cfg_eta_so_exp = identity_l_so(l_so_exp)
cfg_duration_time = Inf
cfg_bound_sim = 10^4

# Test on one simulation
l_so_test = simulate(f, g_all, x0, u, [p_true for i = 1:nb_traj]; on = on, full_timeline = true)
so_test = l_so_test[1]
p_test = p_true
for om in so_test.oml
    so_exp = l_so_exp[1]
    fig = pygmalion_plot()
    title("$(string(p_test)) - l1 dist = $(l1_step(so_exp,so_test))")
    x = to_vec(so_test, "time")
    y = to_vec(so_test, om)
    x = vcat(x0.time, x)
    y = vcat(getfield(x0, Symbol(om)), y)
    x2 = to_vec(so_exp, "time")
    y2 = to_vec(so_exp, om)
    x2 = vcat(x0.time, x2)
    y2 = vcat(getfield(x0, Symbol(om)), y2)
    plt.step(x, y, "o--", color="grey", marker="x", where="post", linewidth = 1.0)
    plt.step(x2, y2, "bo--", marker="x", where="post", linewidth = 1.0)
    current_axis = fig.axes[1]
    ylim(0, maximum(vcat(y,y2))+1)
    xlim(0, 1.01*t_end)
    savefig(str_dir * om * "_sim.png", dpi=480)
    close()
end

verb = false

redirect_to_files(str_dir * "abc_pmc.log", str_dir * "abc_pmc.err") do
    println("Time of acceptance condition : ")
    @time cfg_distance_function(cfg_eta_function(l_so_test), cfg_eta_function(l_so_exp))
	@show cfg_ll_p
	@show cfg_srl_p
    @show p_true
    @show prct_eps, cfg_end_epsilon
    cfg_abc = ConfigurationAbcPopMc(cfg_ll_p, cfg_srl_p, cfg_bl_p, cfg_nbr_particles, cfg_eta_function, cfg_distance_function, cfg_str_statistical_measure, cfg_l_epsilon, cfg_alpha, cfg_kernel_type, true)
    @timev r_abc = abc_pop_mc(f, g_all, x0, u, p_true, mn, on, l_so_exp, cfg_abc; eta_so_exp = cfg_eta_so_exp, duration_time = cfg_duration_time, bound_sim = cfg_bound_sim)
    @show r_abc.nbr_sim
    @show (r_abc.epsilon / norm_y_exp), r_abc.epsilon
    @show r_abc.exec_time 
    
    for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param) \n mean = $(sum(r_abc.weights .* r_abc.mat_p_end[i,:])); std = $(sqrt(r_abc.vec_stdev[i]))")
        plt.hist(r_abc.mat_p_end[i,:], bins = 100, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param).png", dpi=480)
        close()
    end

	for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param) \n mean = $(sum(r_abc.weights .* r_abc.mat_p_end[i,:])); std = $(sqrt(r_abc.vec_stdev[i]))")
        plt.hist(r_abc.mat_p_end[i,:], bins = 50, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param)_2.png", dpi=480)
        close()
    end


	cfg_nbr_p = length(cfg_ll_p)
	for i = 1:cfg_nbr_p
		for j = (i+1):cfg_nbr_p
			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (50, 50), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior.png", dpi=480)
			close()

			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (25, 25), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior_2.png", dpi=480)
			close()
		end
	end

    nbr_sim_rand = 1
    makedir(str_dir * "p_rand")
    for i = 1:nbr_sim_rand
        id_p_rand = rand(1:cfg_nbr_particles)
        vec_p_rand = r_abc.mat_p_end[:,id_p_rand]
        p_rand = create_from_subset(p_true, vec_p_rand, cfg_ll_p)
        so_abc_p_rand = simulate(f, g_all, x0, u, p_rand; mn = mn, on = on, full_timeline = true)
		makedir(str_dir * "p_rand/p$i")
        for om in so_abc_p_rand.oml
            fig = pygmalion_plot()
            title(string(p_rand))
            x = to_vec(so_abc_p_rand, "time")
            y = to_vec(so_abc_p_rand, om)
            plt.step(x, y, "ro--", marker="x", where="post", linewidth = 1.0)
            current_axis = fig.axes[1]
            ylim(0, 100)
            xlim(0, 1.01*t_end)
            savefig(str_dir * "p_rand/p$i/" * om * "_sim.png", dpi=480)
            close()
        end
    end
end

