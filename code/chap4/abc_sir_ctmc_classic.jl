
if VERSION >= v"0.7.0"
    @everywhere using Distributed
    @everywhere using Random
    using DelimitedFiles
end
@everywhere using pygmalion
import LinearAlgebra: norm
include(ENV["PYGMALION_DIR"] * "etc/plot.jl")
include(ENV["PYGMALION_DIR"] * "algorithms/estimation/abc_pop_mc.jl")

function redirect_to_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end

@everywhere str_m = "sir_ctmc"
str_d = "abc_sir_ctmc_dist_int"
str_dir =  create_results_directory(; title = str_d)
@everywhere load_model(str_m)

on = nothing
mn = nothing

str_oml = "X_S,X_I,X_R,R,time"
ll_om = split(str_oml, ",")

# Set-up
@everywhere N_x0 = 100.0
@everywhere s0 = 0.95
@everywhere S_0 = floor(N_x0 * s0)
@everywhere global x0 = State(S_0, N_x0 - S_0, 0.0, 0.0, 0.0)
@show x0
p_true = Parameters(0.12 / N_x0, 0.05)
global const nb_pts_obs = 20
global const t_end = 100.0
h_step = (t_end)/(nb_pts_obs - 1)
tml = 1:nb_pts_obs
global const l_t_obs = 0.0:h_step:t_end
@show length(tml)
g_all = create_observation_function([ObserverModel(str_oml, tml)])
g_obs = create_observation_function([ObserverModel("I,time", tml)])
u = Control(t_end)

nb_traj = 1
so_exp = load_from_obs("database/observations/sir_ctmc/0-12_0-05_1.obs")

# Config ABC
function euclidean(so_sim::Union{SystemObservation, Nothing}, so_exp::::Union{SystemObservation, Nothing})
    if so_sim == nothing return Inf end
    om = "X_I"
    idx_om = om_findfirst(om, so.oml)
    idx_time = om_findfirst("time", so.oml)
    idx_tml = 1
    nb_states = length(so.otll[idx_om])
    for i_state in 1:(nb_states)
        time_state = so.otll[idx_time][i_state][2][1]
        value_obs = (i_state > 1) ? so.otll[idx_om][i_state-1][2][1] : x0.X_I
        last_idx = idx_tml
        #println("State $i_state (t : $time_state, val : $value_obs), : $idx_tml ")
        for i in idx_tml:nb_pts_obs
            if l_t_obs[i] >= time_state  
                break
            end
            l_y_obs[i] = value_obs
            last_idx = (i+1)
            #println("-- push : $i ti : $(l_t_obs[i]) $value_obs")
        end
        idx_tml = last_idx
    end
    #@show idx_tml
    #@show length(l_y_obs), length(l_t_obs)
    return l_y_obs
end

function ss_sir(l_so::Union{SystemObservationList, Nothing})
    mat_sim  = zeros(nb_traj, nb_pts_obs)
    for i = 1:nb_traj
        mat_sim[i,:] = vec_ss_sir(l_so[i])
    end
    return mat_sim
end

function dist_dataset(mat_sim::AbstractArray, mat_exp::AbstractArray)
    d = 0.0
    for j = 1:nb_pts_obs
        d += norm((mat_sim[:,j] - mat_exp[:,j]),2)
    end
    return sqrt(d)
end

cfg_ll_p = ["ki", "kr"]
cfg_ll_p = ["kr"]
cfg_srl_p = [
             #SamplingRule(["ki"], "uniform", (0.005 / N_x0, 0.3 / N_x0)), 
             SamplingRule(["kr"], "uniform", (0.005, 0.2))
            ]
cfg_bl_p = [
            #Bound("ki", 0.0, 0.3 / N_x0), 
            Bound("kr", 0.0, 0.2)
           ]
cfg_nbr_particles = 100
cfg_eta_function = ss_sir
cfg_distance_function = dist_dataset
cfg_str_statistical_measure = "mean"
cfg_end_epsilon = 0.1 * sqrt(dot(ss_sir(l_so_exp), ss_sir(l_so_exp))) 
cfg_l_epsilon = [cfg_end_epsilon]
cfg_alpha = 0.75
cfg_kernel_type = "mv_normal"
cfg_eta_so_exp = ss_sir(l_so_exp)

# Test on one simulation

so_test = l_so_exp[1]
p_test = p_true
for om in so_test.oml
    fig = pygmalion_plot()
    title(string(p_test))
    x = to_vec(so_test, "time")
    y = to_vec(so_test, om)
    x = vcat(x0.time, x)
    y = vcat(getfield(x0, Symbol(om)), y)
    plt.step(x, y, "ro--", marker="x", where="post", linewidth = 1.0)
    if om == "X_I" plt.plot(l_t_obs, vec_ss_sir(so_test), "bo", linewidth = 1.0, ms = 1.0) end
    current_axis = fig.axes[1]
    ylim(0, maximum(y)+1)
    xlim(0, 1.01*t_end)
    savefig(str_dir * om * "_sim.png", dpi=480)
    close()
end

verb = false

#redirect_to_files(str_dir * "abc_pmc.log", str_dir * "abc_pmc.err") do
	@show cfg_ll_p
	@show cfg_srl_p
    @show p_true

    cfg_abc = ConfigurationAbcPopMc(cfg_ll_p, cfg_srl_p, cfg_bl_p, cfg_nbr_particles, cfg_eta_function, cfg_distance_function, cfg_str_statistical_measure, cfg_l_epsilon, cfg_alpha, cfg_kernel_type, true)
    @timev r_abc = abc_pop_mc(f, g_all, x0, u, p_true, mn, on, l_so_exp, cfg_abc; eta_so_exp = cfg_eta_so_exp)
    @show r_abc.nbr_sim
    for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param)")
        plt.hist(r_abc.mat_p_end[i,:], bins = 100, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param).png", dpi=480)
        close()
    end

	for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param)")
        plt.hist(r_abc.mat_p_end[i,:], bins = 50, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param)_2.png", dpi=480)
        close()
    end

#=
	cfg_nbr_p = length(cfg_ll_p)
	for i = 1:cfg_nbr_p
		for j = (i+1):cfg_nbr_p
			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (50, 50), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior.png", dpi=480)
			close()

			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (25, 25), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior_2.png", dpi=480)
			close()
		end
	end
=#

    nbr_sim_rand = 10
    makedir(str_dir * "p_rand")
    for i = 1:nbr_sim_rand
        id_p_rand = rand(1:cfg_nbr_particles)
        vec_p_rand = r_abc.mat_p_end[:,id_p_rand]
        p_rand = create_from_subset(p_true, vec_p_rand, cfg_ll_p)
        so_abc_p_rand = simulate(f, g_all, x0, u, p_rand; mn = mn, on = on, full_timeline = true)
		makedir(str_dir * "p_rand/p$i")
        for om in so_abc_p_rand.oml
            fig = pygmalion_plot()
            title(string(p_rand))
            x = to_vec(so_abc_p_rand, "time")
            y = to_vec(so_abc_p_rand, om)
            plt.step(x, y, "ro--", marker="x", where="post", linewidth = 1.0)
            current_axis = fig.axes[1]
            ylim(0, 100)
            xlim(0, 1.01*t_end)
            savefig(str_dir * "p_rand/p$i/" * om * "_sim.png", dpi=480)
            close()
        end
    end
#end

