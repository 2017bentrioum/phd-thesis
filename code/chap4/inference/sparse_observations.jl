
using Plots
using DelimitedFiles
using MarkovProcesses
load_plots()

load_model("SIR")
set_x0!(SIR, [:S, :I, :R], [95, 5, 0])
set_param!(SIR, [:ki, :kr], [0.0012, 0.05])
time_bound = 150.0
set_time_bound!(SIR, time_bound)
for i = 1:5
    σ = trajectory_from_csv("observations/SIR_event_discrete_obs_$(i).csv", SIR)
    tml_obs = 0:15.0:time_bound
    y_obs = vectorize(σ, :I, tml_obs)
    plot(σ, :I)
    scatter!(tml_obs, y_obs, markershape = :xcross, markersize = 4, linewidth = 4, alpha = 0.9, label = "observations")
    savefig("observations/SIR_time_discrete_sparse_obs_$(i).svg")
    open("observations/SIR_time_discrete_sparse_obs_$(i).csv", write = true) do file_obs
        write(file_obs, "time,I\n")
        writedlm(file_obs, [tml_obs y_obs], ',')
    end
    open("observations/SIR_event_discrete_obs_$(i).csv", write = true) do file_obs
        write(file_obs, "time,I\n")
        writedlm(file_obs, [times(σ) σ.I], ',')
    end
end

load_model("ER")
set_x0!(ER, [:E, :S, :ES, :P], [100, 100, 0, 0])
set_param!(ER, [:k1, :k2, :k3], [1.0, 1.0, 5.0])
time_bound = 0.075
set_time_bound!(ER, time_bound)
for i = 1:5
    σ = trajectory_from_csv("observations/ER_event_discrete_obs_$(i).csv", ER)
    tml_obs = 0:0.0075:time_bound
    y_obs = vectorize(σ, :P, tml_obs)
    plot(σ, :P)
    scatter!(tml_obs, y_obs, markershape = :xcross, markersize = 4, linewidth = 4, alpha = 0.9, label = "observations")
    savefig("observations/ER_time_discrete_sparse_obs_$(i).svg")
    open("observations/ER_time_discrete_sparse_obs_$(i).csv", write = true) do file_obs
        write(file_obs, "time,P\n")
        writedlm(file_obs, [tml_obs y_obs], ',')
    end
    open("observations/ER_event_discrete_obs_$(i).csv", write = true) do file_obs
        write(file_obs, "time,P\n")
        writedlm(file_obs, [times(σ) σ.P], ',')
    end
end

