
using Dates
using Plots
using DelimitedFiles
@everywhere using MarkovProcesses
import LinearAlgebra: dot

@assert 1 <= length(ARGS) <= 2 "Wrong number of arguments"
exp = "abc_ER_l2_dist"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path *= "/code/chap4/inference"
if length(ARGS) == 2
    path_results = ARGS[2]
else
    path_results = path * "/results_$(exp)_$(ARGS[1])_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end

# ER model
load_model("ER")
set_x0!(ER, [:E, :S, :ES, :P], [100, 100, 0, 0])

# Observations
observations = [trajectory_from_csv(path * "/observations/ER_event_discrete_obs_$(i).csv", ER) for i = 1:5]
set_time_bound!(ER, 0.075)

# Parametric model
pm_ER = ParametricModel(ER, (:k3, Uniform(0.0, 100.0)))

# ABC
@everywhere function dist_er(l_sim::Vector{Trajectory}, l_obs)
    min_dist = Inf
    for i = 1:5
        for j = (i+1):5
            current_dist = dist_lp(l_sim[i], l_obs[j], :P; p = 2)
            if current_dist < min_dist
                min_dist = current_dist
            end
        end
    end
    return min_dist
end
bound_sim = convert(Int, parse(Float64, ARGS[1]))
res_abc = abc_smc(pm_ER, observations, dist_er; bound_sim = bound_sim, nbr_particles = 1000, alpha = 0.75, dir_results = path_results)
samples_abc_post = vec(res_abc.mat_p_end)
samples_weights = res_abc.weights

histogram(samples_abc_post, weights = samples_weights, normalize = :density)
savefig(path_results * "/histogram.svg")

