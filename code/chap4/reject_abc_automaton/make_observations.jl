
using Plots
using DelimitedFiles
using MarkovProcesses

load_model("repressilator")
set_x0!(repressilator, [:mRNA1, :mRNA2, :mRNA3], fill(0, 3))
set_x0!(repressilator, [:P1, :P2, :P3], [5, 0, 15])
set_param!(repressilator, [:α, :α0, :β, :n], [200.0, 0.0, 5.0, 4.0])
time_bound = 400.0
set_time_bound!(repressilator, time_bound)

σ = simulate(repressilator)
tml_obs = 0:10.0:time_bound
y_obs = vectorize(σ, :P1, tml_obs)
plot(tml_obs, y_obs)
savefig("observations/repressilator_traj1.svg")
open("observations/repressilator_traj1.csv", write = true) do file_obs
    write(file_obs, "time,P1\n")
    writedlm(file_obs, [tml_obs y_obs], ',')
end

