
using Dates
using Plots
using DelimitedFiles
@everywhere using MarkovProcesses
import LinearAlgebra: dot

@assert length(ARGS) <= 1 "Too much arguments"
exp = "abc_repressilator_vectorize_dist"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path *= "/code/chap4/reject_abc_automaton"
if length(ARGS) == 1
    path_results = ARGS[1]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end

# repressilator model
load_model("repressilator")
set_param!(repressilator, [:α, :α0, :β, :n], [200.0, 0.0, 5.0, 4.0])

# Observations
values, header = readdlm(path * "/observations/repressilator_traj1.csv", ',', header = true)
timeline = values[:,1]
observations = [values[:,2]]
set_time_bound!(repressilator, timeline[end])

# Parametric model
pm_repressilator = ParametricModel(repressilator, (:β, Uniform(0.5, 10.0)), (:n, Uniform(0.5, 10.0)))

# ABC
epsilon = 0.5 * sqrt(dot(observations[1], observations[1]))
println("Final epsilon: $(epsilon)")
@everywhere @eval function dist_repr(l_sim::Vector{Trajectory}, l_obs)
    return euclidean_distance(l_sim[1], :P1, $(timeline), l_obs[1])
end
res_abc = abc_smc(pm_repressilator, observations, dist_repr; nbr_particles = 100, tolerance = epsilon, dir_results = path_results)
samples_abc_post = res_abc.mat_p_end
samples_weights = res_abc.weights

histogram2d(samples_abc_post[1,:], samples_abc_post[2,:], weights = samples_weights, normalize = :density)
savefig(path_results * "/histogram.svg")

