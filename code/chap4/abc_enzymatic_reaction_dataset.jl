
if VERSION >= v"0.7.0"
    @everywhere using Distributed
    @everywhere using Random
    using DelimitedFiles
end
@everywhere using pygmalion
import LinearAlgebra: norm
import Statistics: mean, std
include(ENV["PYGMALION_DIR"] * "etc/plot.jl")
include(ENV["PYGMALION_DIR"] * "algorithms/estimation/abc_pop_mc.jl")

function redirect_to_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end

@everywhere str_m = "enzymatic_reaction"
str_d = "abc_enzymatic_reaction_dataset"
str_dir =  create_results_directory(; title = str_d)
@everywhere load_model(str_m)
on = nothing
mn = nothing
str_oml = "E,S,ES,P,R,time"
ll_om = split(str_oml, ",")

# Set-up
x0 = State(100, 100, 0, 0, 0, 0.0)
p_true = Parameters(1.0, 1.0, 5.0)
const nb_pts_obs = 75
const t_end = 0.075
h_step = (t_end)/(nb_pts_obs - 1)
tml = 1:nb_pts_obs
const l_t_obs = 0.0:h_step:t_end
g_all = create_observation_function([ObserverModel(str_oml, tml)])
g_obs = create_observation_function([ObserverModel("P,time", tml)])
u = Control(t_end)

nb_traj = 5
l_so_exp = SystemObservationList(undef, nb_traj)
#=
for i = 1:nb_traj
    so_i = simulate(f, g_all, x0, u, p_true; on = on, full_timeline = true)
    save_to_obs(so_i, "database/observations/enzymatic_reaction/1_1_5_$i.obs")
end
=#
for i = 1:nb_traj
    l_so_exp[i] = load_from_obs(ENV["PYGMALION_DIR"] * "database/observations/enzymatic_reaction/1_1_5_$i.obs")
end

# Config ABC
# SS functions
@everywhere function vec_ss_er(so::Union{SystemObservation, Nothing}, nb_pts_obs::Int, 
                                l_t_obs::AbstractArray, x0::State)
    if ss_er == nothing return [Inf] * nb_pts_obs end
    om = "P"
    l_y_obs = zeros(nb_pts_obs)
    idx_om = om_findfirst(om, so.oml)
    idx_time = om_findfirst("time", so.oml)
    idx_tml = 1
    nb_states = length(so.otll[idx_om])
    for i_state in 1:(nb_states)
        time_state = so.otll[idx_time][i_state][2][1]
        value_obs = (i_state > 1) ? so.otll[idx_om][i_state-1][2][1] : x0.P
        last_idx = idx_tml
        #println("State $i_state (t : $time_state, val : $value_obs), : $idx_tml ")
        for i in idx_tml:nb_pts_obs
            if l_t_obs[i] >= time_state  
                break
            end
            l_y_obs[i] = value_obs
            last_idx = (i+1)
            #println("-- push : $i ti : $(l_t_obs[i]) $value_obs")
        end
        idx_tml = last_idx
    end
    #@show idx_tml
    #@show length(l_y_obs), length(l_t_obs)
    return l_y_obs
end
@everywhere function ss_er(l_so::Union{SystemObservationList, Nothing}, nb_pts_obs::Int, 
                                l_t_obs::AbstractArray, x0::State)
    nb_traj = length(l_so)
    mat_sim  = zeros(nb_pts_obs, nb_traj)
    for j = 1:nb_traj
        mat_sim[:,j] = vec_ss_er(l_so[j], nb_pts_obs, l_t_obs, x0)
    end
    return mat_sim
end
@everywhere ss_er(l_so::Union{SystemObservationList, Nothing}) = ss_er(l_so, $(nb_pts_obs), $(l_t_obs), $x0)
# Dist functions
@everywhere function min_dist_euclidean(mat_sim::AbstractArray, mat_exp::AbstractArray)
    nb_traj = size(mat_exp)[2]
    d = Inf
    for j1 = 1:nb_traj
        for j2 = 1:nb_traj
            d_current = euclidean_distance(mat_sim[:,j1], mat_exp[:,j2])
            if d_current < d d = d_current end
        end
    end
    return d
end
cfg_ll_p = ["k3"]
cfg_srl_p = [
             SamplingRule(["k3"], "uniform", (0.0, 100.0))
            ]
cfg_bl_p = [
            Bound("k3", 0.0, 100.0)
           ]
cfg_nbr_particles = 1000
cfg_eta_function = ss_er
#cfg_distance_function = dist_dataset
cfg_distance_function = min_dist_euclidean
cfg_str_statistical_measure = "mean"
prct_eps = 0.0
norm_y_exp = sqrt(dot(ss_er(l_so_exp), ss_er(l_so_exp)))
cfg_end_epsilon = prct_eps * norm_y_exp 
cfg_l_epsilon = [cfg_end_epsilon]
cfg_alpha = 0.75
cfg_kernel_type = "mv_normal"
cfg_eta_so_exp = ss_er(l_so_exp)
cfg_duration_time = Inf
cfg_bound_sim = 10^7

# Test on one simulation

l_so_test = simulate(f, g_all, x0, u, [p_true for i = 1:nb_traj]; on = on, full_timeline = true)
so_test = l_so_test[1]
p_test = p_true
for om in so_test.oml
    fig = pygmalion_plot()
    title("$(p_test)\n nb_pts_obs = $(nb_pts_obs)")
    x = to_vec(so_test, "time")
    y = to_vec(so_test, om)
    x = vcat(x0.time, x)
    y = vcat(getfield(x0, Symbol(om)), y)
    plt.step(x, y, "ro--", marker="x", where="post", linewidth = 1.0)
    if om == "P" plt.plot(l_t_obs, vec_ss_er(so_test, nb_pts_obs, l_t_obs, x0), "bo", linewidth = 1.0, ms = 1.0) end
    current_axis = fig.axes[1]
    ylim(0, maximum(y)+1)
    xlim(0, 1.01*t_end)
    savefig(str_dir * om * "_sim.png", dpi=480)
    close()
end

verb = false

redirect_to_files(str_dir * "abc_pmc.log", str_dir * "abc_pmc.err") do
    println("Time of acceptance condition : ")
    @time test_accept = cfg_distance_function(cfg_eta_function(l_so_test), cfg_eta_function(l_so_exp))
    @show test_accept
    @show cfg_ll_p
	@show cfg_srl_p
    @show t_end, nb_pts_obs
    @show p_true
    @show prct_eps, cfg_end_epsilon

    cfg_abc = ConfigurationAbcPopMc(cfg_ll_p, cfg_srl_p, cfg_bl_p, cfg_nbr_particles, cfg_eta_function, cfg_distance_function, cfg_str_statistical_measure, cfg_l_epsilon, cfg_alpha, cfg_kernel_type, true)
    @timev r_abc = abc_pop_mc(f, g_all, x0, u, p_true, mn, on, l_so_exp, cfg_abc; eta_so_exp = cfg_eta_so_exp, duration_time = cfg_duration_time, bound_sim = cfg_bound_sim)
    @show r_abc.nbr_sim
    @show (r_abc.epsilon / norm_y_exp), r_abc.epsilon
    @show r_abc.exec_time 
    
    for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param) \n mean = $(sum(r_abc.weights .* r_abc.mat_p_end[1,:])); std = $(sqrt.(r_abc.vec_stdev)) ")
        plt.hist(r_abc.mat_p_end[i,:], bins = 100, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param).png", dpi=480)
        close()
    end

	for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param) \n mean = $(sum(r_abc.weights .* r_abc.mat_p_end[1,:])); std = $(sqrt.(r_abc.vec_stdev))")
        plt.hist(r_abc.mat_p_end[i,:], bins = 50, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param)_2.png", dpi=480)
        close()
    end

#=
	cfg_nbr_p = length(cfg_ll_p)
	for i = 1:cfg_nbr_p
		for j = (i+1):cfg_nbr_p
			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (50, 50), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior.png", dpi=480)
			close()

			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (25, 25), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior_2.png", dpi=480)
			close()
		end
	end
=#

    nbr_sim_rand = 1
    makedir(str_dir * "p_rand")
    for i = 1:nbr_sim_rand
        id_p_rand = rand(1:cfg_nbr_particles)
        vec_p_rand = r_abc.mat_p_end[:,id_p_rand]
        p_rand = create_from_subset(p_true, vec_p_rand, cfg_ll_p)
        so_abc_p_rand = simulate(f, g_all, x0, u, p_rand; mn = mn, on = on, full_timeline = true)
		makedir(str_dir * "p_rand/p$i")
        for om in so_abc_p_rand.oml
            fig = pygmalion_plot()
            title(string(p_rand))
            x = to_vec(so_abc_p_rand, "time")
            y = to_vec(so_abc_p_rand, om)
            plt.step(x, y, "ro--", marker="x", where="post", linewidth = 1.0)
            current_axis = fig.axes[1]
            ylim(0, 100)
            xlim(0, 1.01*t_end)
            savefig(str_dir * "p_rand/p$i/" * om * "_sim.png", dpi=480)
            close()
        end
    end
end

