
using MarkovProcesses
load_plots()

path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_pics = path * "/pictures/chap4/oscillators"
if !isdir(path_pics) mkdir(path_pics) end

load_model("doping_3way_oscillator")
observe_all!(doping_3way_oscillator)
load_automaton("period_automaton")

A_per = create_period_automaton(doping_3way_oscillator, 320.0, 360.0, 5, :A; ref_mean_tp = 0.01)
sync_doping = doping_3way_oscillator * A_per

for param in [[1.0, 1.0, 1.0], [0.75, 1.0, 1.0], [2.0, 1.0, 1.0]]
    set_time_bound!(sync_doping, 0.5)
    set_x0!(doping_3way_oscillator, [:A, :B, :C], fill(333, 3))
    set_x0!(doping_3way_oscillator, [:DA, :DB, :DC], fill(10, 3))
    set_param!(doping_3way_oscillator, [:rA, :rB, :rC], param)
    σ = simulate(sync_doping)
    plot_periodic_trajectory(A_per, σ, :A, show_tp = true, annot_size = 4, filename = "./doping_3way_rA_$(param[1]).png")
end

