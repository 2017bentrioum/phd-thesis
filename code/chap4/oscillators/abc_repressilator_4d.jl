
@everywhere using MarkovProcesses
using Dates
using Plots
load_plots()

@assert length(ARGS) <= 1 "Too much arguments"
exp = "repressilator"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path *= "/code/chap4/oscillators"
if length(ARGS) == 1
    path_results = ARGS[1]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end

# automaton-ABC ruN
N_periods = 4
ref_mean_tp = 20.0

load_model("repressilator")
observe_all!(repressilator)
set_param!(repressilator, [:α, :β, :n, :α0], [200.0, 2.0, 2.0, 0.0])
set_time_bound!(repressilator, 2N_periods*ref_mean_tp)

L, H = 50.0, 200.0
load_automaton("period_automaton")
A_per = create_period_automaton(repressilator, L, H, N_periods, :P1; ref_mean_tp = ref_mean_tp, error_func = :max_mean_var_relative_error)
sync_repressilator = repressilator * A_per

pm_sync_repressilator = ParametricModel(sync_repressilator, (:α, Uniform(50.0, 5000.0)), (:β, Uniform(0.1, 5.0)), (:n, Uniform(0.5, 5.0)), (:α0, Uniform(0.0, 5.0)))
nbr_pa = 1000
α = 0.5
r = automaton_abc(pm_sync_repressilator; nbr_particles = nbr_pa, tolerance = 0.1, alpha = α, dir_results = path_results)
samples_abc_post = r.mat_p_end

# Histogram
hist1 = histogram2d(samples_abc_post[[1,2],:]', weights = r.weights, xlabel = "α", ylabel = "β", bins = :scott)
hist2 = histogram2d(samples_abc_post[[1,3],:]', weights = r.weights, xlabel = "α", ylabel = "n", bins = :scott)
hist3 = histogram2d(samples_abc_post[[2,3],:]', weights = r.weights, xlabel = "β", ylabel = "n", bins = :scott)
p = plot(hist1, hist2, hist3, layout = 3)
savefig(p, path_results * "/histogram.svg")

