
@everywhere using MarkovProcesses
using Plots
using Dates
load_plots()

@assert length(ARGS) <= 1 "Too much arguments"
exp = "doping_3way_3d"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path *= "/code/chap4/oscillators"
if length(ARGS) == 1
    path_results = ARGS[1]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end

# automaton-ABC run
N_periods = 4
ref_mean_tp = 0.01
load_model("doping_3way_oscillator")
set_x0!(doping_3way_oscillator, [:A, :B, :C], fill(333, 3))
set_x0!(doping_3way_oscillator, [:DA, :DB, :DC], fill(10, 3))
set_time_bound!(doping_3way_oscillator, (N_periods+2)*ref_mean_tp)
load_automaton("period_automaton")
A_per = create_period_automaton(doping_3way_oscillator, 300.0, 360.0, N_periods, :A; ref_mean_tp = ref_mean_tp, error_func = :max_mean_var_relative_error)
sync_doping = doping_3way_oscillator * A_per

pm_sync_doping = ParametricModel(sync_doping, (:rA, Uniform(0.0, 10.0)), (:rB, Uniform(0.0, 10.0)), (:rC, Uniform(0.0, 10.0)))
nbr_pa = 1000
r = automaton_abc(pm_sync_doping; nbr_particles = nbr_pa, tolerance = 0.2, dir_results = path_results)
samples_abc_post = r.mat_p_end

# Histogram
hist1 = histogram2d(samples_abc_post[[1,2],:]', weights = r.weights, xlabel = "rA", ylabel = "rB", bins = :scott)
hist2 = histogram2d(samples_abc_post[[1,3],:]', weights = r.weights, xlabel = "rA", ylabel = "rC", bins = :scott)
hist3 = histogram2d(samples_abc_post[[2,3],:]', weights = r.weights, xlabel = "rB", ylabel = "rC", bins = :scott)
p = plot(hist1, hist2, hist3, layout = 3)
savefig(p, path_results * "/histogram.svg")

