
using MarkovProcesses
load_plots()

path_pics = "./pics_repressilator"
if !isdir(path_pics) mkdir(path_pics) end

load_model("repressilator")
set_param!(repressilator, [:α, :α0, :n, :β], [2000.0, 1.0, 2.0, 5.0])
set_time_bound!(repressilator, 500.0)
observe_all!(repressilator)
load_automaton("period_automaton")
A_per = create_period_automaton(repressilator, 100.0, 300.0, 5, :P1)

sync_repressilator = repressilator * A_per
σ = simulate(sync_repressilator)
plot(σ; A = A_per, filename = path_pics * "/traj_full.png")
plot_periodic_trajectory(A_per, σ, :P1, filename = path_pics * "/traj_automaton.png", annot_size = 7)

@show σ.state_lha_end[:n]
@show σ.state_lha_end[:mean_tp]
@show σ.state_lha_end[:var_tp]
@show σ.state_lha_end[:d]

