
using MarkovProcesses
using Plots
load_plots()

path = haskey(ENV, "BENTRIOU_THESIS") ? ENV["BENTRIOU_THESIS"] : "."
path_pics = path * "/pictures/chap4/oscillators"
if !isdir(path_pics) mkdir(path_pics) end

load_model("repressilator")
set_time_bound!(repressilator, 150.0)
observe_all!(repressilator)

plots = []

# Varying β
set_param!(repressilator, [:α, :β, :n, :α0], [200.0, 2.0, 2.0, 0.0])
l_beta = [0.5, 1.0, 2.0, 4.0]
for beta in l_beta
    set_param!(repressilator, :β, beta)
    σ = simulate(repressilator)
    p = plot(times(σ), σ.P1, title = "β = $(beta)", linetype = :steppost, palette = :lightrainbow, label = "", alpha = 0.5)
    for i = 1:4
        σ = simulate(repressilator)
        plot!(p, times(σ), σ.P1, linetype = :steppost, label = "", alpha = 0.5)
    end
    push!(plots, p)
end
p_layout =  plot(plots...; layout = 4, dpi = 480)
savefig(p_layout, path_pics * "/sim_repressilator_varying_beta.png")
empty!(plots)

# Varying α
set_param!(repressilator, [:α, :β, :n, :α0], [200.0, 2.0, 2.0, 0.0])
l_alpha = [50.0, 200.0, 1000.0, 4000.0]
for alpha in l_alpha
    set_param!(repressilator, :α, alpha)
    σ = simulate(repressilator)
    p = plot(times(σ), σ.P1, title = "α = $(alpha)", linetype = :steppost, palette = :lightrainbow, label = "", alpha = 0.5)
    for i = 1:4
        σ = simulate(repressilator)
        plot!(p, times(σ), σ.P1, linetype = :steppost, label = "", alpha = 0.5)
    end
    push!(plots, p)
end
p_layout =  plot(plots...; layout = 4, dpi = 480)
savefig(p_layout, path_pics * "/sim_repressilator_varying_alpha.png")
empty!(plots)

# Varying α0
set_param!(repressilator, [:α, :β, :n, :α0], [200.0, 2.0, 2.0, 0.0])
l_alpha0 = [0.0, 0.01, 0.1, 1.0]
for alpha0 in l_alpha0
    set_param!(repressilator, :α0, alpha0)
    σ = simulate(repressilator)
    p = plot(times(σ), σ.P1, title = "α0 = $(alpha0)", linetype = :steppost, palette = :lightrainbow, label = "", alpha = 0.5)
    for i = 1:4
        σ = simulate(repressilator)
        plot!(p, times(σ), σ.P1, linetype = :steppost, label = "", alpha = 0.5)
    end
    push!(plots, p)
end
p_layout =  plot(plots...; layout = 4, dpi = 480)
savefig(p_layout, path_pics * "/sim_repressilator_varying_alpha0.png")
empty!(plots)

# Varying n
set_param!(repressilator, [:α, :β, :n, :α0], [200.0, 2.0, 2.0, 0.0])
l_n = [0.5, 1.0, 2.0, 5.0]
for n in l_n
    set_param!(repressilator, :n, n)
    σ = simulate(repressilator)
    p = plot(times(σ), σ.P1, title = "n = $(n)", linetype = :steppost, palette = :lightrainbow, label = "", alpha = 0.5)
    for i = 1:4
        σ = simulate(repressilator)
        plot!(p, times(σ), σ.P1, linetype = :steppost, label = "", alpha = 0.5)
    end
    push!(plots, p)
end
p_layout =  plot(plots...; layout = 4, dpi = 480)
savefig(p_layout, path_pics * "/sim_repressilator_varying_n.png")
empty!(plots)

