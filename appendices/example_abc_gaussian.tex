
\chapter{A simple analytical example of ABC inference}

\label{appendix:ex_abc}

Let us illustrate the ABC method and kernel density estimation with a simple example. We consider $n$ i.i.d observations $\yexp = (y^{(1)}, \ldots, y^{(n)})$ from $\mathcal{N}(\mu, \sigma_0^2)$. We want to estimate $\theta = \mu$ via Approximate Bayesian Computation ($\sigma_0 = 1.0$ is known). The prior is uniform over $[-1, 1]$ ($p(\theta) \propto 1$). 


\section{Computation of the true posterior and ABC posterior}

In this case, the different posteriors can be computed analytically, and the mean estimator is a sufficient statistic. We denote $\displaystyle \eta(\yexp) = \yexpbar = \frac{1}{n} \sum_{i=1}^n y^{(i)}$.

First, let us compute the true posterior distribution.

\begin{equation*}
    \begin{aligned}
        p(\theta | \yexp) &\propto p(\yexp | \theta) p(\theta) \\
                          &\propto \prod_{i=1}^n p(y^{(i)} | \theta) \\
                          & \propto e^{\frac{1}{2\sigma_0^2} (\sum_{i=1}^n (y^{(i)} - \theta)^2)} \\
                          & \propto e^{\frac{1}{2\sigma_0^2} (\sum_{i=1}^n y^{(i)})^2}  
                               e^{\frac{n\yexpbar^2}{2\sigma_0^2}} 
                               \underbrace{e^{\frac{n}{2\sigma_0^2} (\yexpbar - \theta)^2}}_{\propto \text{ density of } \mathcal{N}(\yexpbar, (\frac{\sigma_0}{\sqrt{n}})^2)}
    \end{aligned}
\end{equation*}

So $p(.| \yexp) \sim \mathcal{N}(\yexpbar, (\frac{\sigma_0}{\sqrt{n}})^2)$ by the factorisation theorem and $\eta$ is a sufficient statistic. The prior being uniform, $p(.| \theta) \sim \mathcal{N}(\theta, (\frac{\sigma_0}{\sqrt{n}})^2)$. 

Let us compute the ABC posterior distribution. ABC can be seen as a regular bayesian method with an approximate likelihood \ref{rmk:approx_lh}: 

\begin{equation*}
    p_{ABC}^{\eps}(\yexp | \theta) = \int \mathbb{1}(||y - \yexp|| \leq \eps) p(y|\theta) dy
\end{equation*}

\begin{equation*}
    \piabceps(\theta | y_obs) \propto p_{ABC}^{\eps}(\yexp | \theta) p(\theta)
\end{equation*}

Let us compute the approximate likelihood: 

\begin{equation*}
    \begin{aligned}
        p_{ABC}^{\eps}(\yexp | \theta)  & = p_{ABC}^{\eps}(\yexpbar | \theta) \\
                                            & = \int_{\R} \mathbb{1}(||\overline{y} - \yexpbar||) p(\overline{y}|\theta) d\overline{y} \\
                                            & = \int_{\yexpbar - \epsilon}^{\yexpbar + \epsilon} 
                                            p_{\mathcal{N}(\theta, (\frac{\sigma_0}{\sqrt{n}})^2)}(\overline{y}) d\overline{y}
    \end{aligned}
\end{equation*}

Then $p_{ABC}^{\eps}(\yexp | \theta) = \Prob(Y_{\theta} \leq \yexpbar + \epsilon) - \Prob(Y_{\theta} \leq \yexpbar - \epsilon)$
where $Y_{\theta} \sim \mathcal{N}(\theta, (\frac{\sigma_0}{\sqrt{n}})^2)$.

Hence, by uniform prior $\piabceps(\theta | \yexp) \propto p_{ABC}^{\eps}(\yexp | \theta)$.

By Fubini and by recognising a law $\mathcal{N}(\theta, (\frac{\sigma_0}{\sqrt{n}})^2)$,

\begin{equation*}
    \begin{aligned}
    \int_{\R} p_{ABC}^{\epsilon}(\yexp | \theta) d\theta    & = \int_{\R} (\int_{\yexpbar - \epsilon}^{\yexpbar + \epsilon}
                                                                    \frac{\sqrt{n}}{\sqrt{2\pi}} e^{\frac{n}{2\sigma_0^2}(\overline{y}-\theta)^2} d\overline{y}) d\theta \\
                                                                    & = 2 \epsilon
    \end{aligned}
\end{equation*}

In conclusion: 

\begin{equation*}
    \piabceps(\theta | \yexp) = \frac{\Prob(Y_{\theta} \leq \yexpbar + \epsilon) - \Prob(Y_{\theta} \leq \yexpbar - \epsilon)}{2\epsilon}
\end{equation*}

If $Z \sim \piabceps(.| \yexpbar)$, 

\begin{equation*}
    \begin{aligned}
        \E[Z] & = \int_{\R} \theta \pi_{ABC,\epsilon}(.| \yexpbar) d\theta \\
                &= \frac{1}{2\epsilon} \int_{\R} \theta (\int_{\yexpbar - \epsilon}^{\yexpbar + \epsilon}
                   \frac{\sqrt{n}}{\sqrt{2\pi}} e^{\frac{n}{2\sigma_0^2}(\overline{y}-\theta)^2} d\overline{y}) d\theta \\ 
                & = \frac{1}{2\epsilon} \int_{\yexpbar - \epsilon}^{\yexpbar + \epsilon} 
                    (\int_{\R} \theta \frac{\sqrt{n}}{\sqrt{2\pi}} e^{\frac{n}{2\sigma_0^2}(\overline{y}-\theta)^2}) d\overline{y}  
    \end{aligned}
\end{equation*}

We recognise the expectation of $\mathcal{N}(\overline{y}, \frac{\sigma_0^2}{n})$, then:

\begin{equation*}
    \begin{aligned}
        \E[Z] & = \frac{1}{2\epsilon}\int_{\yexpbar - \epsilon}^{\yexpbar + \epsilon} \overline{y} d\overline{y}
                & = \frac{1}{4\epsilon}((\yexpbar + \epsilon)^2 - (\yexpbar - \epsilon)^2)
                & = \yexpbar
    \end{aligned}
\end{equation*}

By the same ideas of computations: 

\begin{equation*}
    \begin{aligned}
        \E[Z^2] & = \frac{1}{2\epsilon} \int_{\yexpbar - \epsilon}^{\yexpbar + \epsilon} 
                      (\int_{\R} \theta^2 \frac{\sqrt{n}}{\sqrt{2\pi}} e^{\frac{n}{2\sigma_0^2}(\overline{y}-\theta)^2}) d\overline{y} \\
                  & = \frac{1}{2\epsilon} \int_{\yexpbar - \epsilon}^{\yexpbar + \epsilon} \overline{y}^2 \frac{\sigma_0^2}{n} d\overline{y} \\
                  & = \frac{\sigma_0^2}{n} + \frac{1}{6\epsilon}((\yexpbar + \epsilon)^3 - (\yexpbar - \epsilon)^3) \\
                  & = \frac{\sigma_0^2}{n} + \yexpbar^2 + \frac{\epsilon^2}{3}
    \end{aligned}
\end{equation*}

It follows: $\displaystyle \Var[Z] = \E[Z^2] - \E[Z]^2 = \frac{\sigma_0^2}{n} + \frac{\epsilon^2}{3} \xrightarrow[\epsilon \to 0]{} \frac{\sigma_0^2}{n}$

In conclusion, 

\begin{equation*}
    \begin{aligned}
        \E[Z] &= \yexpbar \\ 
        \Var[Z] &= \frac{\sigma_0^2}{n} + \frac{\epsilon^2}{3}
    \end{aligned}
\end{equation*}

\section{Simulations}

We run the ABC-SMC algorithm with 100, 1000 and 10000 particles.

With $\epsilon = 0.01$.

\begin{figure}[H]
    \centering
    \begin{tabular}{cc}    
        \includegraphics[width=0.46\textwidth]{pictures/appendix_ex_abc/abc_gaussian/abc_gauss_1000_1E-2.png} & 
        \includegraphics[width=0.46\textwidth]{pictures/appendix_ex_abc/abc_gaussian/abc_gauss_10000_1E-2.png}
    \end{tabular}
    \caption{Weighted histograms of ABC. In green the true posterior distribution, 
    in blue the true ABC posterior and in red the estimated ABC posterior with gaussian kernel. On the left: 1000 particles. On the right: 10000 particles.}.
    \label{fig:abc_eps_0_01}
\end{figure}

With $\epsilon = 0.1$.

\begin{figure}[H]
    \centering
    \begin{tabular}{cc}    
        \includegraphics[width=0.46\textwidth]{pictures/appendix_ex_abc/abc_gaussian/abc_gauss_1000_1E-1.png} &
        \includegraphics[width=0.46\textwidth]{pictures/appendix_ex_abc/abc_gaussian/abc_gauss_10000_1E-1.png}
    \end{tabular}
    \caption{Weighted histograms of ABC run. In green the true posterior distribution, 
    in blue the true ABC posterior and in red the estimated ABC posterior with gaussian kernel. On the left: 1000 particles. On the right: 10000 particles.}
    \label{fig:abc_eps_0_1}
\end{figure}

Here is a detailed run with $\epsilon = 0.2$ and 1000 particies. 

\begin{figure}[H]
    \centering
    \begin{tabular}{cc}
        \includegraphics[width=0.46\textwidth]{pictures/appendix_ex_abc/abc_gaussian/abc_gauss_1000_2E-1.png} &
        \includegraphics[width=0.46\textwidth]{pictures/appendix_ex_abc/abc_gaussian/abc_gauss_1000_2E-1_resampled.png}
    \end{tabular}
    \caption{Histogram of ABC run with 1000 particles. In green the true posterior distribution, in blue the true ABC posterior and in red the estimated ABC posterior with gaussian kernel. On the left: multinomial resampling with the weights. On the right: weighted estimator.}.
    \label{fig:abc_eps_0_2_resampling}
\end{figure}

%Let $\displaystyle \overline{X_n} = \sum_{i=1}^N w_i X_i$ be an estimator of $\yexpbar$ where $N$ is the number of particles and $X_i \sim \pi_{ABC,\epsilon}(.|\yexpbar)$ i.i.d. observations.
%On the graph we can see the error between the estimation and the true value of $\yexpbar$ is not anormal according to the Tchebychev inequality. Also, the Kolmogorov-Smirnov test has great p-values over $\approx 10$ runs with the resampled ABC samples (and always refused with the ABC samples without resampling).


